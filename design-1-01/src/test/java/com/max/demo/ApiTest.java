package com.max.demo;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.HashMap;

/**
 * Created by lilei on 2021/9/10.
 */
@Slf4j
public class ApiTest {

    /**
     * 模拟发放优惠券
     */
    @Test
    public void test1() {

        PrizeController prizeController = new PrizeController();

        System.out.println("\r\n 01 模拟发放优惠券测试\r\n");
        // 01 模拟发放优惠券测试
        AwardReq req01 = AwardReq.builder()
                .uId("10001")
                .awardType(1)
                .awardNumber("EGM1023938910232121323432")
                .bizId("791098764902132").build();
        AwardRes awardRes01 = prizeController.awardToUser(req01);
        log.info("请求参数：{}", JSON.toJSON(req01));
        log.info("测试结果：{}", JSON.toJSON(awardRes01));

        System.out.println("\r\n 02 模拟方法实物商品\r\n");
        // 02 模拟方法实物商品
        AwardReq req02 = AwardReq.builder()
                .uId("10001")
                .awardType(2)
                .awardNumber("9820198721311")
                .bizId("1023000020112221113").build();
        req02.setExtMap(new HashMap<String, String>() {{ // 收货人信息
            put("consigneeUserName", "谢飞机");
            put("consigneeUserPhone", "15200292123");
            put("consigneeUserAddress", "吉林省.长春市.双阳区.XX街道.檀溪苑小区.#18-2109");
        }});
        AwardRes awardRes02 = prizeController.awardToUser(req02);
        log.info("请求参数：{}", JSON.toJSON(req02));
        log.info("测试结果：{}", JSON.toJSON(awardRes02));

        System.out.println("\r\n 03 第三方兑换卡(爱奇艺)\r\n");
        AwardReq req03 = AwardReq.builder()
                .uId("10001")
                .awardType(3)
                .awardNumber("AQY1xjkUodl8LO975GdfrYUio").build();
        AwardRes awardRes03 = prizeController.awardToUser(req03);
        log.info("请求参数：{}", JSON.toJSON(req03));
        log.info("测试结果：{}", JSON.toJSON(awardRes03));

    }


}
