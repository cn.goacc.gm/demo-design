package com.max.demo;

import lombok.*;

import java.util.Map;

/**
 * Created by lilei on 2021/9/10.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AwardReq {

    /** 01 用户唯一ID*/
    private String uId;
    /** 02 奖品类型(可以用枚举定义)；1优惠券、2实物商品、3第三方兑换卡(爱奇艺)*/
    private Integer awardType;
    /** 03 奖品编号；sku、couponNumber、cardId*/
    private String awardNumber;
    /** 04 业务ID，防重复*/
    private String bizId;
    /** 05 扩展信息*/
    private Map<String, String> extMap;

}
