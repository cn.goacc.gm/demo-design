package com.max.demo;

import lombok.*;

/**
 * Created by lilei on 2021/9/10.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AwardRes {

    /** 01 编码*/
    private String code;
    /** 02 描述*/
    private String info;
}
