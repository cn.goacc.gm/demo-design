package com.max.demo;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by lilei on 2021/10/9. <p>
 * 模拟审核服务  <p>
 * auth          审核流程  <p>
 * queryAuthInfo 查询审核信息(时间) <p>
 */
public class AuthService {

    private static Map<String, Date> authMap = new ConcurrentHashMap<>();

    public static Date queryAuthInfo(String uId, String orderId) {
        return authMap.get(uId.concat(orderId));
    }

    public static void auth(String uId, String orderId) {
        authMap.put(uId.concat(orderId), new Date());
    }

}
