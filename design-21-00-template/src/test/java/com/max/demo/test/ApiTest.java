package com.max.demo.test;

import com.max.demo.NetMall;
import com.max.demo.impl.JDNetMall;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * Created by lilei on 2021/11/16. <br>
 */
@Slf4j
public class ApiTest {


    /**
     * 模版模式: <br>
     * 核心: <br>
     * 01 通过抽象类中定义 抽象方法的执行顺序 (登录->爬虫商品信息->渲染海报) <br>
     * 实现: <br>
     * 01 {@link NetMall#generateGoodsPoster(java.lang.String)} 核心 <br>
     * 总结: <br>
     * 01 将抽象方法定义只有子类访问 <br>
     * 02 开发中使用不多  <br>
     * 03 解决将子类通用方法，放到父类进行优化
     */
    @Test
    public void test_NetMall() {
        NetMall netMall = new JDNetMall("1000001", "*******");
        String base64 = netMall.generateGoodsPoster("https://item.jd.com/100008348542.html");
        log.info("测试结果：{}", base64);
    }

}
