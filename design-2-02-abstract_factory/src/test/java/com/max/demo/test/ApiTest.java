package com.max.demo.test;

import com.max.demo.CacheService;
import com.max.demo.factory.JDKProxy;
import com.max.demo.factory.impl.EGMCacheAdapter;
import com.max.demo.factory.impl.IIRCacheAdapter;
import com.max.demo.impl.CacheServiceImpl;
import org.junit.Test;

/**
 * Created by lilei on 2021/9/10.
 * <p>
 * 02 抽象工厂
 */
public class ApiTest {

    /**
     * 实现方式:
     * 01 有两个redis集群(EGM,IIR) 问题:两者方法名不同 <p>
     * 02 定义适配接⼝(统一方法名)
     * ICacheAdapter.java <p>
     * 03 EGM和IIR 实现ICacheAdapter接口 (⚠️注意: 逻辑处理成IIR集群的方法) (解决了差异化的方法名)
     * EGMICacheAdapter.java   IIRCacheAdapter.java <p>
     * 04 创建JDKInvocationHandler类 实现 InvocationHandler 接口 重写invoke方法  <p>
     * 该类参数(Object proxy, Method method, Object[] args) <p>
     * 05 创建JDKProxy.class 目的生成代理类
     */

    /**
     * 目的:
     * 抽象⼯⼚模式与⼯⼚⽅法模式虽然主要意图都是为了解决，接⼝选择问题。但
     * 在实现上，抽象⼯⼚是⼀个中心工厂，创建其他⼯厂的模式
     * (工厂类的上方再封装一层) 所有工厂类的子类
     */

    /**
     * 总结:
     * 工厂模式: 通过标识来生成不同的产品
     * 抽象工厂模式: 所要解决的问题就是在一个产品族,存在多个不同类型的产品(Redis集群、操作系 统)情况下
     * `接口选择的问题`。⽽这种场景在业务开发中也是⾮常多见的，只不不过可能有时候没有 将它们抽象化出来
     */


    @Test
    public void test_CacheService() throws Exception {
        // 通过传入
        CacheService proxy_EGM = JDKProxy.getProxy(CacheServiceImpl.class, new EGMCacheAdapter());
        proxy_EGM.set("user_name_01", "木子");
        String val01 = proxy_EGM.get("user_name_01");
        System.out.println("测试结果：" + val01);

        CacheService proxy_IIR = JDKProxy.getProxy(CacheServiceImpl.class, new IIRCacheAdapter());
        proxy_IIR.set("user_name_01", "木子");
        String val02 = proxy_IIR.get("user_name_01");
        System.out.println("测试结果：" + val02);

    }

}
