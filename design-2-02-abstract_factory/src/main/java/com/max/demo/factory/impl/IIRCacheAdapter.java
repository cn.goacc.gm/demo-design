package com.max.demo.factory.impl;

import com.max.demo.factory.ICacheAdapter;
import com.max.demo.matter.IIR;

import java.util.concurrent.TimeUnit;

/**
 * Created by lilei on 2021/9/10.
 * <p>
 * 02 实现适配器接口
 * <p>
 * 逻辑处理成IIR集群的方法
 */
public class IIRCacheAdapter implements ICacheAdapter {

    private final IIR iir = new IIR(); //✨

    @Override
    public String get(String key) {
        return iir.get(key);
    }

    @Override
    public void set(String key, String value) {
        iir.set(key, value);
    }

    @Override
    public void set(String key, String value, long timeout, TimeUnit timeUnit) {
        iir.setExpire(key, value, timeout, timeUnit);
    }

    @Override
    public void del(String key) {
        iir.del(key);
    }
}
