package com.max.demo.factory.impl;

import com.max.demo.factory.ICacheAdapter;
import com.max.demo.matter.EGM;

import java.util.concurrent.TimeUnit;

/**
 * Created by lilei on 2021/9/10.
 * <p>
 * 03 实现适配器接口
 * <p>
 * 逻辑处理成EGM集群的方法
 */
public class EGMCacheAdapter implements ICacheAdapter {

    private final EGM egm = new EGM();

    @Override
    public String get(String key) {
        return egm.gain(key);
    }

    @Override
    public void set(String key, String value) {
        egm.set(key, value);
    }

    @Override
    public void set(String key, String value, long timeout, TimeUnit timeUnit) {
        egm.setEx(key, value, timeout, timeUnit);
    }

    @Override
    public void del(String key) {
        egm.delete(key);
    }

}
