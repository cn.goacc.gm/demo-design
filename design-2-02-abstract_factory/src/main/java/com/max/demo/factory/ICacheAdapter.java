package com.max.demo.factory;

import java.util.concurrent.TimeUnit;

/**
 * Created by lilei on 2021/9/10.
 * <p>
 * 01 定义适配接⼝
 * <p>
 * 统一方法名
 */
public interface ICacheAdapter {

    String get(String key);

    void set(String key, String value);

    void set(String key, String value, long timeout, TimeUnit timeUnit);

    void del(String key);

}
