package com.max.demo.test;

import com.max.demo.QuestionBankController;
import org.junit.Test;

/**
 * Created by lilei on 2021/9/28. <p>
 */
public class ApiTest {


    /**
     * 步骤:
     * 01 将需要克隆的类 implements Cloneable <p>
     * 02 在内部类中将大量需要new的对象定义出来
     * 03 重写Object中的clone方法
     * 04 将逻辑写入clone方法
     * 05 创建一个克隆的类 每次获取该类不同的属性信息时候, 调用该类clone方法
     */

    /**
     * 总结:
     * 01 原型模式的使⽤用频率不是很⾼
     * 02 解决的是: 创建复杂对象,避免重复的初始化操作
     * 03 缺点如果对象中包括了循环引⽤的克隆，以及类中深度使用对象的克隆，都会使此模式变得异常麻烦
     */
    @Test
    public void test_QuestionBank() throws CloneNotSupportedException {
        QuestionBankController questionBankController = new QuestionBankController();
        System.out.println(questionBankController.createPaper("花花", "1000001921032"));
        System.out.println(questionBankController.createPaper("豆豆", "1000001921051"));
        System.out.println(questionBankController.createPaper("大宝", "1000001921987"));
    }
}
