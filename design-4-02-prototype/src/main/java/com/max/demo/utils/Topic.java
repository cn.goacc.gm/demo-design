package com.max.demo.utils;

import lombok.*;

import java.util.Map;

/**
 * Created by lilei on 2021/9/28. <p>
 * 选择题
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Topic {

    private Map<String, String> option;  // 选项；A、B、C、D
    private String key;           // 答案；B

}
