package com.max.demo.utils;

import java.util.*;

/**
 * Created by lilei on 2021/9/28. <p>
 * 题⽬目选项乱序操作⼯工具包
 */
public class TopicRandomUtil {


    /**
     * 乱序Map元素，记录对应答案key
     *
     * @param option 题目
     * @param key    答案
     * @return Topic 乱序后 {A=c., B=d., C=a., D=b.}
     */
    // keySet按顺序循环所有选项(顺便循环了打乱顺序的List):
    // 程序走的方式: value:按顺序存储; key: 按照keyList(打乱后的顺序存储)
    // 获取正确答案的方式: 正确答案的Key==KeySet(按照顺序的)中的Key:  \
    // \ keyList(打乱后的)的Key就等于新的正确答案的Key() \
    // \ 下面的 optionNew.put(randomKey, option.get(next)); \
    // \ randomKey: 随机的Key, option.get(next):KeySet(按顺序)Key的值
    static public Topic random(Map<String, String> option, String key) {
        Set<String> keySet = option.keySet();
        ArrayList<String> keyList = new ArrayList<String>(keySet);
        Collections.shuffle(keyList);
        HashMap<String, String> optionNew = new HashMap<String, String>();
        int idx = 0;
        String keyNew = "";
        for (String next : keySet) {
            String randomKey = keyList.get(idx++);
            if (key.equals(next)) {
                keyNew = randomKey;
            }
            optionNew.put(randomKey, option.get(next));
        }

        return new Topic(optionNew, keyNew);
    }


}
