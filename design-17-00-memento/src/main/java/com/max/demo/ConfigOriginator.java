package com.max.demo;

import lombok.Data;

/**
 * Created by lilei on 2021/10/11. <p>
 * 记录者类
 */
@Data
public class ConfigOriginator {

    private ConfigFile configFile;


    /**
     * 保存备忘录的时候会创建⼀个备忘录信息，并返回去，交给管理员处理
     *
     * @return
     */
    public ConfigMemento saveMemento() {
        return new ConfigMemento(configFile);
    }

    /**
     * 获取的之后并不是直接返回，而是把备忘录的信息交给现在的配置文件 <p>
     * this.configFile, 这部分需要注意
     *
     * @param memento
     */
    public void getMemento(ConfigMemento memento) {
        this.configFile = memento.getConfigFile();
    }
}
