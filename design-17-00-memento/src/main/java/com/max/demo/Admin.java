package com.max.demo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by lilei on 2021/10/11. <p>
 */
public class Admin {

    private int cursorIdx = 0;
    private List<ConfigMemento> mementoList = new ArrayList<>();
    private Map<String, ConfigMemento> mementoMap = new ConcurrentHashMap<>();

    public void append(ConfigMemento memento) {
        mementoList.add(memento); // 保存备忘录类(里面存放者配置信息)
        mementoMap.put(memento.getConfigFile().getVersionNo(), memento); // 保存版本
        cursorIdx++;
    }

    /**
     * 撤销
     *
     * @return
     */
    public ConfigMemento undo() {
        if (--cursorIdx <= 0) return mementoList.get(0); // cursorIdx减1了
        return mementoList.get(cursorIdx);
    }

    public ConfigMemento redo() {
        if (++cursorIdx > mementoList.size()) return mementoList.get(mementoList.size() - 1);
        return mementoList.get(cursorIdx);
    }

    public ConfigMemento get(String versionNo) {
        return mementoMap.get(versionNo);
    }

}
