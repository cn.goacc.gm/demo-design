package com.max.demo;

import lombok.*;

import java.util.Date;

/**
 * Created by lilei on 2021/10/11. <p>
 * 01 配置信息类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConfigFile {
    /** 版本号 */
    private String versionNo;
    /** 内容 */
    private String content;
    /** 时间 */
    private Date dateTime;
    /** 操作人 */
    private String operator;

}
