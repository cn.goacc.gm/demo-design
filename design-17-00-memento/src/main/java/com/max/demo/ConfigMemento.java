package com.max.demo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by lilei on 2021/10/11. <p>
 * 02 备忘录类 <P>
 * 备忘录是对原有配置类的扩展，可以设置和获取配置信息
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConfigMemento {

    private ConfigFile configFile;

}
