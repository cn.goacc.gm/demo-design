package com.max.demo.test;

import com.alibaba.fastjson.JSON;
import com.max.demo.Admin;
import com.max.demo.ConfigFile;
import com.max.demo.ConfigMemento;
import com.max.demo.ConfigOriginator;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.Date;

/**
 * Created by lilei on 2021/10/11. <p>
 */
@Slf4j
public class ApiTest {

    /**
     * 备忘录模式
     * 核心: <br>
     * 01 {@link ConfigFile} 配置信息 (需要记录的信息) <br>
     * 02 {@link ConfigMemento} 备忘录类 对原有类的扩展 (可理解成统一处理记录信息)  <br>
     * 03 {@link ConfigOriginator} 记录者类 (可理解成git暂存区) <br>
     * 本质是通过修改属性字段{@link ConfigOriginator#getConfigFile()}不断重新赋值来实现 <br>
     * 04 {@link Admin} 回滚和前进实现: 本质是操作 {@code ++cursorIdx}和{@code --cursorIdx} 来实现 <br>
     * 05 {@link ConfigOriginator#saveMemento()} 注意该方法是返回一个备忘录类 {@link ConfigMemento}
     */
    @Test
    public void test() {

        Admin admin = new Admin();

        ConfigOriginator configOriginator = new ConfigOriginator(); // 记录者(可理解成git缓存区)

        configOriginator.setConfigFile(new ConfigFile("1000001", "配置内容A=哈哈", new Date(), "小傅哥"));
        admin.append(configOriginator.saveMemento()); // 保存配置

        configOriginator.setConfigFile(new ConfigFile("1000002", "配置内容A=嘻嘻", new Date(), "小傅哥"));
        admin.append(configOriginator.saveMemento()); // 保存配置

        configOriginator.setConfigFile(new ConfigFile("1000003", "配置内容A=么么", new Date(), "小傅哥"));
        admin.append(configOriginator.saveMemento()); // 保存配置

        configOriginator.setConfigFile(new ConfigFile("1000004", "配置内容A=嘿嘿", new Date(), "小傅哥"));
        admin.append(configOriginator.saveMemento()); // 保存配置


        /**
         *  01 因为undo操作的同一对象 每调用一次undo()方法回滚一次 (注意: 实际操作的是下标cursorIdx)
         *  02 注意: getMemento方法的实现是重新赋值对象,而不是直接获取
         *  03 configOriginator.getConfigFile() 方法获取的是重新赋值的对象
         */
        // 历史配置(回滚)
        configOriginator.getMemento(admin.undo());
        log.info("历史配置(回滚)undo：{}", JSON.toJSONString(configOriginator.getConfigFile()));

        // 历史配置(回滚)
        configOriginator.getMemento(admin.undo());
        log.info("历史配置(回滚)undo：{}", JSON.toJSONString(configOriginator.getConfigFile()));

        /**
         * 这里也是操作下标 ++cursorIdx 重新赋值
         */
        // 历史配置(前进)
        configOriginator.getMemento(admin.redo());
        log.info("历史配置(前进)redo：{}", JSON.toJSONString(configOriginator.getConfigFile()));

        /**
         * (这里的需求: 通过版本号获取配置) mementoMap: 查看历史版本
         * 存放结构(version,ConfigMemento)
         * 在保存设置时候操作(saveMemento()): version=ConfigMemento.ConfigFile.versionNo
         */
        // 历史配置(获取)
        configOriginator.getMemento(admin.get("1000002"));
        log.info("历史配置(获取)get：{}", JSON.toJSONString(configOriginator.getConfigFile()));

    }


}
