package com.max.demo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by lilei on 2021/11/16. <br>
 * 基本活动信息
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ActivityInfo {
    /** 活动ID */
    private String activityId;
    /** 活动名称 */
    private String activityName;
    /** 活动状态 */
    private Enum<Status> status;
    /** 开始时间 */
    private Date beginTime;
    /** 结束时间 */
    private Date endTime;

}
