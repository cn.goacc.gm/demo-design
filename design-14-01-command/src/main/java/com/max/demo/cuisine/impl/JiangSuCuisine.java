package com.max.demo.cuisine.impl;

import com.max.demo.cook.ICook;
import com.max.demo.cuisine.ICuisine;

/**
 * Created by lilei on 2021/10/9. <p>
 * 江苏（苏菜）
 */
public class JiangSuCuisine implements ICuisine {

    private ICook cook;

    public JiangSuCuisine(ICook cook) {
        this.cook = cook;
    }

    public void cook() {
        cook.doCooking();
    }
}
