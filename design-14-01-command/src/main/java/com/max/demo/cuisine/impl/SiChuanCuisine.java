package com.max.demo.cuisine.impl;

import com.max.demo.cook.ICook;
import com.max.demo.cuisine.ICuisine;

/**
 * Created by lilei on 2021/10/9. <p>
 * 四川（川菜）
 */
public class SiChuanCuisine implements ICuisine {

    private ICook cook;

    public SiChuanCuisine(ICook cook) {
        this.cook = cook;
    }

    public void cook() {
        cook.doCooking();
    }
}
