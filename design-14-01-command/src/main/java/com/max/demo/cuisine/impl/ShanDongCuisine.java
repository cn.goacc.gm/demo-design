package com.max.demo.cuisine.impl;

import com.max.demo.cook.ICook;
import com.max.demo.cuisine.ICuisine;

/**
 * Created by lilei on 2021/10/9. <p>
 * 山东（鲁菜）
 */
public class ShanDongCuisine implements ICuisine {

    private ICook cook;

    public ShanDongCuisine(ICook cook) {
        this.cook = cook;
    }

    public void cook() {
        cook.doCooking();
    }
}
