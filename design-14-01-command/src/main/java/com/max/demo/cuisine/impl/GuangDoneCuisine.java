package com.max.demo.cuisine.impl;

import com.max.demo.cook.ICook;
import com.max.demo.cuisine.ICuisine;

/**
 * Created by lilei on 2021/10/9. <p>
 */
public class GuangDoneCuisine implements ICuisine {
    private ICook cook;

    public GuangDoneCuisine(ICook cook) {
        this.cook = cook;
    }

    @Override
    public void cook() {
        cook.doCooking();
    }

}
