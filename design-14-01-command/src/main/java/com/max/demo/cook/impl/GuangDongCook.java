package com.max.demo.cook.impl;

import com.max.demo.cook.ICook;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by lilei on 2021/10/9. <p>
 */
@Slf4j
public class GuangDongCook implements ICook {

    public void doCooking() {
        log.info("广东厨师，烹饪鲁菜，宫廷最大菜系，以孔府风味为龙头");
    }
}
