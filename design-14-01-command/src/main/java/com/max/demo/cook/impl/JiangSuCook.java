package com.max.demo.cook.impl;

import com.max.demo.cook.ICook;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by lilei on 2021/10/9. <p>
 */
@Slf4j
public class JiangSuCook implements ICook {
    public void doCooking() {
        log.info("江苏厨师，烹饪苏菜，宫廷第二大菜系，古今国宴上最受人欢迎的菜系。");
    }
}
