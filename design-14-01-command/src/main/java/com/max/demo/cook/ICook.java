package com.max.demo.cook;

/**
 * Created by lilei on 2021/10/9. <p>
 * 厨师接⼝
 */
public interface ICook {

    void doCooking();
}
