package com.max.demo.cook.impl;

import com.max.demo.cook.ICook;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by lilei on 2021/10/9. <p>
 */
@Slf4j
public class SiChuanCook implements ICook {

    public void doCooking() {
        log.info("四川厨师，烹饪川菜，中国最有特色的菜系，也是民间最大菜系。");
    }
}
