package com.max.demo.test;

import com.max.demo.XiaoEr;
import com.max.demo.cook.impl.GuangDongCook;
import com.max.demo.cook.impl.JiangSuCook;
import com.max.demo.cook.impl.ShanDongCook;
import com.max.demo.cook.impl.SiChuanCook;
import com.max.demo.cuisine.ICuisine;
import com.max.demo.cuisine.impl.GuangDoneCuisine;
import com.max.demo.cuisine.impl.JiangSuCuisine;
import com.max.demo.cuisine.impl.ShanDongCuisine;
import com.max.demo.cuisine.impl.SiChuanCuisine;
import org.junit.Test;

/**
 * Created by lilei on 2021/10/9. <p>
 */
public class ApiTest {


    /**
     * 命令模式:
     * 01 开始过程中不常用
     * 02 目的是将逻辑实现与操作请求进行分离,降低耦合
     * 03 目的将两个业务相互组合 (菜品和厨师), `每一个具体的命令都有具体的实现`
     * <p>
     * 总结:
     * 将业务分为三大块: 命令(菜品)、实现(厨师)、调用者(小二)  满足单一职责
     * 缺点: 扩展很多实现类,需要进行管理
     */
    @Test
    public void test() {
        // 菜系 + 厨师；广东（粤菜）、江苏（苏菜）、山东（鲁菜）、四川（川菜）
        ICuisine guangDoneCuisine = new GuangDoneCuisine(new GuangDongCook());
        JiangSuCuisine jiangSuCuisine = new JiangSuCuisine(new JiangSuCook());
        ShanDongCuisine shanDongCuisine = new ShanDongCuisine(new ShanDongCook());
        SiChuanCuisine siChuanCuisine = new SiChuanCuisine(new SiChuanCook());

        // 点单
        XiaoEr xiaoEr = new XiaoEr();
        xiaoEr.order(guangDoneCuisine);
        xiaoEr.order(jiangSuCuisine);
        xiaoEr.order(shanDongCuisine);
        xiaoEr.order(siChuanCuisine);

        // 下单
        xiaoEr.placeOrder();

    }

}
