package com.max.demo.test;

import com.alibaba.fastjson.JSON;
import com.max.demo.Activity;
import com.max.demo.ActivityController;
import com.max.demo.ActivityFactory;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * Created by lilei on 2021/10/8. <p>
 */
@Slf4j
public class ApiTest {

    private ActivityController activityController = new ActivityController();


    /**
     * 享元模式:
     * 共享通用对象,统一抽离作为共享对象使⽤
     * <p>
     * 实现:
     * 01 通过享元工⼚来进行管理这部分独立的对象和共享的对象，
     * - 通过 map 结构存放已经从库表或者接⼝中查询到的数据，存放到内存中，⽤于下次可以直接获取
     * - 当然也有些时候为了了分布式的获取，会把数据存放到redis中 {@link ActivityFactory}
     * 02 通过在控制层 查询库存信息 ,并不断的修改返回给调用方法 {@link ActivityController}
     * <p>
     * 总结:
     * 01 在有大量重复对象可复用的情况下使用
     * 02 当然除了这种设计的减少内存的使⽤用优点外，也有它带来的缺点\
     * \ 在一些复杂的业务处理理场景，很不容易区分出内部和外部状态，就像我们活动信息部分与库存变化部分。\
     * \ 如果不不能很好的拆分，就 会把享元⼯工⼚厂设计的非常混乱，难以维护。
     */
    @Test
    public void test_queryActivityInfo() throws InterruptedException {
        for (int idx = 0; idx < 10; idx++) {
            Long req = 10001L;
            Activity activity = activityController.queryActivityInfo(req);
            log.info("测试结果：{} {}", req, JSON.toJSONString(activity));
            Thread.sleep(1200);
        }
    }

}
