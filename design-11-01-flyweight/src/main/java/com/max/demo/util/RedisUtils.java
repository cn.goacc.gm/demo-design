package com.max.demo.util;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by lilei on 2021/10/8. <p>
 * 查看库存的变化
 */
public class RedisUtils {

    private ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);

    private AtomicInteger stock = new AtomicInteger(0);

    public RedisUtils() {
        scheduledExecutorService.scheduleAtFixedRate(() -> {
            // 模拟库存消耗
            stock.addAndGet(1); // 值加一
        }, 0, 100000, TimeUnit.MICROSECONDS);// (要执行的任务,第一次执行延迟,周期性延迟,时间单位(毫秒))

    }

    public int getStockUsed() {
        return stock.get();
    }


}
