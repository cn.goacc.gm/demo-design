package com.max.demo;

import com.max.demo.util.RedisUtils;

/**
 * Created by lilei on 2021/10/8. <p>
 */
public class ActivityController {

    private RedisUtils redisUtils = new RedisUtils();

    public Activity queryActivityInfo(Long id) {
        Activity activity = ActivityFactory.getActivity(id);
        // 模拟从Redis中获取库存变化信息
        Stock stock = new Stock(1000, redisUtils.getStockUsed());
        activity.setStock(stock); // ✨ 活动信息固定不变, 变化的只是库存数量
        return activity;
    }

}
