package com.max.demo;

import lombok.*;

import java.util.Date;

/**
 * Created by lilei on 2021/10/8. <p>
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Activity {
    /** 活动ID */
    private Long id;
    /** 活动名称 */
    private String name;
    /** 活动描述 */
    private String desc;
    /** 开始时间 */
    private Date startTime;
    /** 结束时间 */
    private Date stopTime;
    /** 活动库存 */
    private Stock stock;

}
