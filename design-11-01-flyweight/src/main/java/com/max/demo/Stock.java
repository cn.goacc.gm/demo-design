package com.max.demo;

import lombok.*;

/**
 * Created by lilei on 2021/10/8. <p>
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Stock {
    /** 库存总量 */
    private int total;
    /** 库存已用 */
    private int used;
}
