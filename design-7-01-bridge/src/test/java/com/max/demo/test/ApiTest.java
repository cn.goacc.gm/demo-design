package com.max.demo.test;

import com.max.demo.pay.channel.Pay;
import com.max.demo.pay.channel.WxPay;
import com.max.demo.pay.channel.ZfbPay;
import com.max.demo.pay.mode.PayFaceMode;
import com.max.demo.pay.mode.PayFingerprintMode;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * Created by lilei on 2021/9/29. <p>
 */
public class ApiTest {


    /**
     * 桥接设计模式
     * 核心: 一个抽象类和接口: 分属两个不同的业务(抽象类: 划帐   接口: 风控校验)
     * 通过抽象类的构造方法将两者连接起来
     * <p>
     * 实现:
     * <p>
     * 01 创建桥接的一方 IPayMode(校验方式)接口
     * 02 并实现该接口(刷脸⽀付、指纹支付、密码支付)
     * 03 创建支付类型桥接抽象类pay: \
     * \ protected属性的桥接接口(IPayMode) 并提供有参构造 \
     * \ 创建transfer()抽象方法(业务方法: 划帐)
     * 04 继承抽象类: 实现不同的划账服务(支付宝、微信)
     * 05 控制层使用: 通过抽象类 子类的实例创建 接入不同的 接口实现 做到桥接的目的
     */
    @Test
    public void test_pay() {
        System.out.println("\r\n模拟测试场景;微信⽀支付、⼈人脸⽅方式。");
        Pay wxPay = new WxPay(new PayFaceMode());
        wxPay.transfer("weixin_1092033111", "100000109893", new
                BigDecimal(100));
        System.out.println("\r\n模拟测试场景;⽀支付宝⽀支付、指纹⽅方式。");
        Pay zfbPay = new ZfbPay(new PayFingerprintMode());
        zfbPay.transfer("jlu19dlxo111", "100000109894", new BigDecimal(100));

    }

}
