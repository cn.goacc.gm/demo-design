package com.max.demo.pay.mode;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by lilei on 2021/9/29. <p>
 */
@Slf4j
public class PayCypher implements IPayMode {
    @Override
    public boolean security(String uId) {
        log.info("密码支付，⻛控校验环境安全");
        return true;
    }
}
