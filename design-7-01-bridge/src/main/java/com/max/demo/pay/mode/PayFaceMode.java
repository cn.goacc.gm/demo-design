package com.max.demo.pay.mode;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by lilei on 2021/9/29. <p>
 */
@Slf4j
public class PayFaceMode implements IPayMode {
    @Override
    public boolean security(String uId) {
        log.info("⼈脸支付,风控校验脸部识别");

        return true;
    }
}
