package com.max.demo.pay.mode;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by lilei on 2021/9/29. <p>
 */
@Slf4j
public class PayFingerprintMode implements IPayMode {

    @Override
    public boolean security(String uId) {
        log.info("指纹⽀支付，⻛风控校验指纹信息");
        return true;
    }

}
