package com.max.demo.test;

import sun.jvm.hotspot.HelloWorld;

/**
 * Created by lilei on 2021/10/8. <p>
 */
public class ApiTest {

    /**
     * 外观模式:
     * 主要解决的是降低调⽤方的使⽤接⼝的 `复杂逻辑组合`
     * <p>
     * 示例: 白名单
     * 通过自定义注解和AOP对接口进行拦截 校验白名单
     * <p>
     * 拦截逻辑: {@link com.max.demo.DoJoinPoint } <p>
     * 使用: {@link com.max.demo.controller.HelloWorldController}
     */
    public void test_Api() {

    }

}
