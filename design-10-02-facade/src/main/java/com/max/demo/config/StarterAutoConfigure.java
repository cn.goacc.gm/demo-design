package com.max.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by lilei on 2021/10/8. <p>
 * 获取yml文件配置信息
 */
@Configuration // 声明当前类是配置类
@ConditionalOnClass(StarterService.class) // 当给定的类名在类路径上存在，则实例化 `当前Bean`
@EnableConfigurationProperties(StarterServiceProperties.class) // 使使用 @ConfigurationProperties 注解的类生效。
public class StarterAutoConfigure {

    @Autowired
    private StarterServiceProperties properties;

    @Bean  // 放入容器中
    @ConditionalOnMissingBean // 当给定的类名在类路径上不存在，则实例化当前Bean (StarterService)
    @ConditionalOnProperty(prefix = "max.door", value = "enabled", havingValue = "true")
        // 控制配置类是否生效
    StarterService starterService() {
        return new StarterService(properties.getUserStr());
    }

}
