package com.max.demo.config;

/**
 * Created by lilei on 2021/10/8. <p>
 * 将配置字段信息进行分割(该对象放入容器中)
 * `映射对象与服务对象分开`
 */
public class StarterService {

    private String userStr;

    public StarterService(String userStr) {
        this.userStr = userStr;
    }

    public String[] split(String separatorChar) {
        return this.userStr.split(separatorChar);
    }

}
