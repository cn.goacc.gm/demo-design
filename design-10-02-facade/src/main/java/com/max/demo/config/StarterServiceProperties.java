package com.max.demo.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Created by lilei on 2021/10/8. <p>
 * yml文件字段映射对象
 */
@ConfigurationProperties("max.door") // 指定yml配置中的前缀
public class StarterServiceProperties {

    private String userStr;

    public String getUserStr() {
        return userStr;
    }

    public void setUserStr(String userStr) {
        this.userStr = userStr;
    }

}
