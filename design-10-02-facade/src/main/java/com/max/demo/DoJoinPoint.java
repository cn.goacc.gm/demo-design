package com.max.demo;

import com.alibaba.fastjson.JSON;
import com.max.demo.annotation.DoMax;
import com.max.demo.config.StarterService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * Created by lilei on 2021/10/8. <p>
 */
@Aspect // 定义切面类
@Component
@Slf4j
public class DoJoinPoint {

    @Autowired
    private StarterService starterService;

    // 定义切面，这里采用的是注解路径，也就是所有的加入这个注解的⽅法都会被切面进⾏管理   (都会调用aopPoint方法)
    @Pointcut("@annotation(com.max.demo.annotation.DoMax)")
    public void aopPoint() {
    }

    @Around("aopPoint()")  // `@Around`环绕通知  (对带有@DoMax注解进行切面，并获取注解属性值)
    public Object doRouter(ProceedingJoinPoint jp) throws Throwable {
        //获取内容
        Method method = getMethod(jp); // 01 获取注解下的 方法对象 (通过ProceedingJoinPoint对象获取 方法名称 and 方法参数)
        DoMax door = method.getAnnotation(DoMax.class); // 02 获取注解 对象
        //获取字段值
        String keyValue = getFiledValue(door.key(), jp.getArgs()); //(注解Key值,方法参数)
        log.info("itstack door handler method：{} value：{}", method.getName(), keyValue);
        if (null == keyValue || "".equals(keyValue)) return jp.proceed(); // jp.proceed 返回方法返回值
        //配置内容
        String[] split = starterService.split(",");
        //白名单过滤
        for (String str : split) {
            if (keyValue.equals(str)) {
                return jp.proceed(); // 返回方法返回值
            }
        }
        //拦截
        return returnObject(door, method);
    }

    private Method getMethod(JoinPoint jp) throws NoSuchMethodException {
        Signature sig = jp.getSignature();
        MethodSignature methodSignature = (MethodSignature) sig; // 获取方法签名信息 (方法名,参数)
        return getClass(jp).getMethod(methodSignature.getName(), methodSignature.getParameterTypes());
    }

    private Class<? extends Object> getClass(JoinPoint jp) throws NoSuchMethodException {
        return jp.getTarget().getClass();
    }

    //返回对象
    private Object returnObject(DoMax doGate, Method method) throws IllegalAccessException, InstantiationException {
        Class<?> returnType = method.getReturnType(); // 获取返回类型(UserInfo)
        String returnJson = doGate.returnJson(); // 获取拦截json信息
        if ("".equals(returnJson)) {
            return returnType.newInstance(); // 创建一个空的无参 UserInfo
        }
        return JSON.parseObject(returnJson, returnType); // json串转对象
    }

    //获取属性值
    private String getFiledValue(String filed, Object[] args) {
        String filedValue = null;
        for (Object arg : args) {
            try {
                if (null == filedValue || "".equals(filedValue)) {
                    filedValue = BeanUtils.getProperty(arg, filed); // (Object bean, String name) // 属性值都以String返回
                } else {
                    break;
                }
            } catch (Exception e) {
                if (args.length == 1) { // 实际
                    return args[0].toString();
                }
            }
        }
        return filedValue;
    }

}