package com.max.demo.engine;

import com.max.demo.aggregates.TreeRich;
import com.max.demo.vo.EngineResult;

import java.util.Map;

/**
 * Created by lilei on 2021/9/29. <p>
 */
public interface IEngine {

    EngineResult process(final Long treeId, final String userId, TreeRich treeRich, final Map<String, String> decisionMatter);

}
