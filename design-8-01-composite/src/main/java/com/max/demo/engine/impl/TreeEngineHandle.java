package com.max.demo.engine.impl;

import com.max.demo.aggregates.TreeRich;
import com.max.demo.engine.EngineBase;
import com.max.demo.vo.EngineResult;
import com.max.demo.vo.TreeNode;

import java.util.Map;

/**
 * Created by lilei on 2021/9/29. <p>
 */
public class TreeEngineHandle extends EngineBase {

    @Override
    public EngineResult process(Long treeId, String userId, TreeRich treeRich, Map<String, String> decisionMatter) {
        // 决策流程
        TreeNode treeNode = engineDecisionMaker(treeRich, treeId, userId, decisionMatter);
        // 决策结果
        return new EngineResult(userId, treeId, treeNode.getTreeNodeId(), treeNode.getNodeValue());
    }


}
