package com.max.demo.logic.impl;

import com.max.demo.logic.BaseLogic;

import java.util.Map;

/**
 * Created by lilei on 2021/9/29. <p>
 */
public class UserAgeFilter extends BaseLogic {
    @Override
    public String matterValue(Long treeId, String userId, Map<String, String> decisionMatter) {
        return decisionMatter.get("age");
    }
}
