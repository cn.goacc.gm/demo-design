package com.max.demo.vo;

import lombok.*;

/**
 * 树根信息
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TreeRoot {
    private Long treeId;         //规则树ID
    private Long treeRootNodeId; //规则树根ID
    private String treeName;     //规则树名称

}
