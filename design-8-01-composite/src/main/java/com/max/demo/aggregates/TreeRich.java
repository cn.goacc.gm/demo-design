package com.max.demo.aggregates;

import com.max.demo.vo.TreeNode;
import com.max.demo.vo.TreeRoot;
import lombok.*;

import java.util.Map;

/**
 * Created by lilei on 2021/9/29. <p>
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TreeRich {

    private TreeRoot treeRoot;                          //树根信息
    private Map<Long, TreeNode> treeNodeMap;        //树节点ID -> 子节点

}
