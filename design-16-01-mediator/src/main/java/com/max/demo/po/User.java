package com.max.demo.po;

import lombok.*;

import java.util.Date;

/**
 * Created by lilei on 2021/10/11. <p>
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private Long id;
    private String name;
    private Integer age;
    private Date createTime;
    private Date updateTime;
}
