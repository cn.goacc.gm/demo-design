package com.max.demo.po;

import lombok.*;

import java.util.Date;

/**
 * Created by lilei on 2021/10/11. <p>
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class School {
    private Long id;
    private String name;
    private String address;
    private Date createTime;
    private Date updateTime;
}
