package com.max.demo.dao;

import com.max.demo.po.School;

/**
 * Created by lilei on 2021/10/11. <p>
 */
public interface ISchoolDao {

    School querySchoolInfoById(Long treeId);
}
