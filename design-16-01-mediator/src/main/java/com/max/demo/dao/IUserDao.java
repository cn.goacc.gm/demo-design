package com.max.demo.dao;

import com.max.demo.po.User;

/**
 * Created by lilei on 2021/10/11. <p>
 */
public interface IUserDao {
    User queryUserInfoById(Long id);

}
