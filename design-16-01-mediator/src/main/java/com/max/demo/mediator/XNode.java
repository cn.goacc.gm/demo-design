package com.max.demo.mediator;

import lombok.*;

import java.util.Map;

/**
 * Created by lilei on 2021/10/11. <p>
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class XNode {
    private String namespace;
    private String id;
    private String parameterType;
    private String resultType;
    private String sql;
    private Map<Integer, String> parameter;
}
