package com.max.demo.mediator;

/**
 * Created by lilei on 2021/10/11. <p>
 * 03  定义SqlSessionFactory接⼝口
 */
public interface SqlSessionFactory {
    SqlSession openSession();
}
