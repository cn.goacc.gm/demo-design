package com.max.demo.mediator;

/**
 * Created by lilei on 2021/10/11. <p>
 * 04 SqlSessionFactory具体实现类
 */
public class DefaultSqlSessionFactory implements SqlSessionFactory {

    private final Configuration configuration;

    public DefaultSqlSessionFactory(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public SqlSession openSession() { // 当开启 SqlSession 时会进行返回⼀个 DefaultSqlSession
        return new DefaultSqlSession(configuration.connection,
                configuration.mapperElement);
    }
}
