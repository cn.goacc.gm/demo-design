package com.max.demo.test;

import com.alibaba.fastjson.JSON;
import com.max.demo.mediator.DefaultSqlSessionFactory;
import com.max.demo.mediator.SqlSession;
import com.max.demo.mediator.SqlSessionFactory;
import com.max.demo.mediator.SqlSessionFactoryBuilder;
import com.max.demo.po.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.io.Resources;
import org.junit.Test;

import java.io.Reader;
import java.util.List;

/**
 * Created by lilei on 2021/10/11. <p>
 */
@Slf4j
public class ApiTest {


    /**
     * 中介者模式 <p>
     * 核心: 解决的就是复杂功能应用之间的重复调用，在这中间添加一层中介者包装服务，对外提供简单、通⽤、易扩展的服务能力  <p>
     * 实现(ORM框架):  <p>
     * 01 {@link SqlSessionFactoryBuilder} 用来关联对象之间的引用关系  <p>
     * 02 {@link SqlSessionFactory } impl {@link DefaultSqlSessionFactory} 封装到一个类向外提供服务 (中介者)  <p>
     */
    @Test
    public void test_queryUserInfoById() {
        String resource = "mybatis-config-datasource.xml";
        Reader reader;
        try {
            reader = Resources.getResourceAsReader(resource);
            SqlSessionFactory sqlMapper = new SqlSessionFactoryBuilder().build(reader);

            SqlSession session = sqlMapper.openSession();
            try {
                User user = session.selectOne("com.max.demo.dao.IUserDao.queryUserInfoById", 1L);
                log.info("测试结果：{}", JSON.toJSONString(user));
            } finally {
                session.close();
                reader.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void test_queryUserList() {
        String resource = "mybatis-config-datasource.xml";
        Reader reader;
        try {
            reader = Resources.getResourceAsReader(resource);
            SqlSessionFactory sqlMapper = new SqlSessionFactoryBuilder().build(reader);

            SqlSession session = sqlMapper.openSession();
            try {
                User req = new User();
                req.setAge(18);
                List<User> userList = session.selectList("com.max.demo.dao.IUserDao.queryUserList", req);
                log.info("测试结果：{}", JSON.toJSONString(userList));
            } finally {
                session.close();
                reader.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
