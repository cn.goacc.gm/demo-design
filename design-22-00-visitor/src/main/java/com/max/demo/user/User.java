package com.max.demo.user;

import com.max.demo.visitor.Visitor;

/**
 * Created by lilei on 2021/11/16. <br>
 * 定义⽤户抽象类
 */
public abstract class User {
    /** 姓名 */
    public String name;
    /** 身份；重点班、普通班 | 特级教师、普通教师、实习教师 */
    public String identity;
    /** 班级 */
    public String clazz;

    public User(String name, String identity, String clazz) {
        this.name = name;
        this.identity = identity;
        this.clazz = clazz;
    }

    /**
     * 核心访问方法 (用来将自身(数据对象) 传递到访问者中)
     */
    public abstract void accept(Visitor visitor);

}
