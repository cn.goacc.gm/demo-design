package com.max.demo.user.impl;

import com.max.demo.visitor.Visitor;
import com.max.demo.user.User;

import java.math.BigDecimal;

/**
 * Created by lilei on 2021/11/16. <br>
 */
public class Teacher extends User {
    public Teacher(String name, String identity, String clazz) {
        super(name, identity, clazz);
    }
    // 升本率
    public double entranceRatio() {
        return BigDecimal.valueOf(Math.random() * 100).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }
    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

}
