package com.max.demo.user.impl;

import com.max.demo.visitor.Visitor;
import com.max.demo.user.User;

/**
 * Created by lilei on 2021/11/16. <br>
 * 学生
 */
public class Student extends User {

    public Student(String name, String identity, String clazz) {
        super(name, identity, clazz);
    }

    public int ranking() {
        return (int) (Math.random() * 100);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
