package com.max.demo.visitor;

import com.max.demo.user.impl.Student;
import com.max.demo.user.impl.Teacher;

/**
 * Created by lilei on 2021/11/16. <br>
 * visit方法重载
 */
public interface Visitor {

    // 访问学生信息
    void visit(Student student);

    // 访问老师信息
    void visit(Teacher teacher);

}
