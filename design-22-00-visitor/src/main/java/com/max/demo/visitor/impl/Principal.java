package com.max.demo.visitor.impl;

import com.max.demo.user.impl.Student;
import com.max.demo.user.impl.Teacher;
import com.max.demo.visitor.Visitor;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by lilei on 2021/11/16. <br>
 * 校长
 */
@Slf4j
public class Principal implements Visitor {
    @Override
    public void visit(Student student) {
        log.info("学生信息 姓名：{} 班级：{}", student.name, student.clazz);
    }

    @Override
    public void visit(Teacher teacher) {
        log.info("学生信息 姓名：{} 班级：{} 升学率：{}", teacher.name, teacher.clazz, teacher.entranceRatio());
    }
}
