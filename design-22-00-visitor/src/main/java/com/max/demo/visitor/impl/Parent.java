package com.max.demo.visitor.impl;

import com.max.demo.user.impl.Student;
import com.max.demo.user.impl.Teacher;
import com.max.demo.visitor.Visitor;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by lilei on 2021/11/16. <br>
 * 家长
 */
@Slf4j
public class Parent implements Visitor {
    @Override
    public void visit(Student student) {
        log.info("学生信息 姓名：{} 班级：{} 排名：{}", student.name, student.clazz, student.ranking());
    }

    @Override
    public void visit(Teacher teacher) {
        log.info("老师信息 姓名：{} 班级：{} 级别：{}", teacher.name, teacher.clazz, teacher.identity);
    }
}
