package com.max.demo;

import com.max.demo.user.User;
import com.max.demo.user.impl.Student;
import com.max.demo.user.impl.Teacher;
import com.max.demo.visitor.Visitor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lilei on 2021/11/16. <br>
 * 数据看板
 */
public class DataView {

    List<User> userList = new ArrayList<>();

    public DataView() {
        userList.add(new Student("谢飞机", "重点班", "一年一班"));
        userList.add(new Student("windy", "重点班", "一年一班"));
        userList.add(new Student("大毛", "普通班", "二年三班"));
        userList.add(new Student("Shing", "普通班", "三年四班"));
        userList.add(new Teacher("BK", "特级教师", "一年一班"));
        userList.add(new Teacher("娜娜Goddess", "特级教师", "一年一班"));
        userList.add(new Teacher("dangdang", "普通教师", "二年三班"));
        userList.add(new Teacher("泽东", "实习教师", "三年四班"));
    }



    /**
     * 展示
     * 参数是Visitor(访问者: 家长Parent)
     * 在调用 user.accept(visitor) (数据: 学生Student)   实际: Student.accept(Parent)
     * 数据调用(学生): 实现: visitor.visit(this);    Parent(家长).visit(this)       this:学生数据  打印一条学生数据
     *
     * 核心: 通过往 数据对象 中传递不同的 访问者 (数据对象和访问者都有实现)
     * 做到在数据对象 调用accept方法时候 将自身(数据对象) 作为参数传递给了 访问者
     */
    public void show(Visitor visitor) {
        for (User user : userList) {
            user.accept(visitor);
        }
    }


}
