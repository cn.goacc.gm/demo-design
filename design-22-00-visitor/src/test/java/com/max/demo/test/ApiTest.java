package com.max.demo.test;

import com.max.demo.DataView;
import com.max.demo.user.User;
import com.max.demo.user.impl.Student;
import com.max.demo.user.impl.Teacher;
import com.max.demo.visitor.Visitor;
import com.max.demo.visitor.impl.Parent;
import com.max.demo.visitor.impl.Principal;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * Created by lilei on 2021/11/16. <br>
 */
@Slf4j
public class ApiTest {

    /**
     * 访问者设计模式
     * 核心: <br>
     * 01 在原有的数据结构下,增加易变的 业务访问逻辑 <br>
     * 02 固定的信息、不同的访问者(相同的数据、不同的业务访问逻辑) <br>
     * <p>
     * 实现: <br>
     * 01 创建数据对象: <br>
     * - 01 定义数据抽象类 {@link User}:  ${@link User#accept(com.max.demo.visitor.Visitor)} <br>
     * - 该抽象方法目的 传递数据对象(自身(Student、Teacher实现))  注意参数: 访问者接口的实现{@link Visitor} <br>
     * - 02 数据对象的实现(学生、老师) {@link Student#ranking()}排名、{@link Teacher#entranceRatio()}升学率、 <br>
     * - (增加易变的 业务访问逻辑) 家长->关注排名、校长->关注升学率 <br>
     * 02 创建访问者 <br>
     * - 01 定义访问者接口 {@link Visitor} 方法重载: {@link Visitor#visit(com.max.demo.user.impl.Student)}、{@link Visitor#visit(com.max.demo.user.impl.Teacher)} <br>
     * - 02 访问者实现: {@link Parent}家长、{@link Principal}校长、 实现接口方法: 通过传递过来的数据对象 调用数据对象中的{@link Student#ranking()}排名或{@link Teacher#entranceRatio()}升学率、方法  <br>
     * 03 创建数据看板{@link DataView} <br>
     * - 01 初始化数据对象 <br>
     * - 02 {@link DataView#show(com.max.demo.visitor.Visitor)} 往数据对象中 传递访问者 <br>
     * - 03 {@link Student#accept(com.max.demo.visitor.Visitor)} 将自身传递给访问者(数据对象传递给访问者) <br>
     * 04 调用:  <br>
     * - 通过传递不同的方法者({@link Visitor}的实现) 达到不同的数据信息 <br>
     * <p>
     * 总结: <br>
     * 01 不必为了不同类型的访问者,增加if判断和类的强制转换
     */
    @Test
    public void test() {
        DataView dataView = new DataView();

        log.info("\r\n家长视角访问：");
        dataView.show(new Parent());     // 家长

        log.info("\r\n校长视角访问：");
        dataView.show(new Principal());  // 校长
    }


}
