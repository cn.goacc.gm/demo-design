package com.max.demo;

/**
 * Created by lilei on 2021/9/29. <p>
 */
public interface HandlerInterceptor {

    boolean preHandle(String request, String response, Object handler);

}
