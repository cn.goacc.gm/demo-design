package com.max.demo.test;

import com.max.demo.SsoInterceptor;
import org.junit.Test;

/**
 * Created by lilei on 2021/9/29. <p>
 */
public class ApiTest {

    @Test
    public void test_sso() {
        SsoInterceptor ssoInterceptor = new SsoInterceptor();
        String request = "1successhuahua";
        boolean success = ssoInterceptor.preHandle(request, "ewcdqwt40liuiu", "t");
        System.out.println("登录校验：" + request + (success ? " 放行" : " 拦截"));
    }

}
