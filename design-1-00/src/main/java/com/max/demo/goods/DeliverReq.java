package com.max.demo.goods;

import lombok.*;

/**
 * Created by lilei on 2021/9/10.<p>
 * 交付信息
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DeliverReq {
    private String userName;              // 用户姓名
    private String userPhone;             // 用户手机
    private String sku;                   // 商品SKU
    private String orderId;               // 订单ID
    private String consigneeUserName;     // 收货人姓名
    private String consigneeUserPhone;    // 收货人手机
    private String consigneeUserAddress;  // 收获人地址
}
