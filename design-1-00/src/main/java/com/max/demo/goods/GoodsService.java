package com.max.demo.goods;

import com.alibaba.fastjson.JSON;
import com.max.demo.coupon.CouponResult;

/**
 * Created by lilei on 2021/9/10. <p>
 * 02 模拟优惠券服务
 */
public class GoodsService {
    public Boolean deliverGoods(DeliverReq req) {
        System.out.println("模拟发货实物商品一个：" + JSON.toJSONString(req));
        return true;
    }

}
