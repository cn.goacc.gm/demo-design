package com.max.demo.coupon;

import lombok.*;

/**
 * Created by lilei on 2021/9/10.
 *
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CouponResult {
    private String code; // 编码
    private String info; // 描述
}
