package com.max.demo.card;

/**
 * Created by lilei on 2021/9/10.
 * <p> 03 模拟爱奇艺会员卡服务</p>
 */
public class IQiYiCardService {

    public void grantToken(String bindMobileNumber, String cardId) {
        System.out.println("模拟发放爱奇艺会员卡一张：" + bindMobileNumber + "，" + cardId);
    }
}
