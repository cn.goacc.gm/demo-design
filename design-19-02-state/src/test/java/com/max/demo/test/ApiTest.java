package com.max.demo.test;

import com.alibaba.fastjson.JSON;
import com.max.demo.ActivityService;
import com.max.demo.Result;
import com.max.demo.StateHandler;
import com.max.demo.Status;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * Created by lilei on 2021/11/16. <br>
 */
@Slf4j
public class ApiTest {

    /**
     * 状态设计模式: <br>
     * 核心: 处理掉大量if-else <br>
     * 实现: <br>
     * 01 {@link Status}抽象类: 定义了各种操作接口(提审、审核、拒审等) <br>
     * 02 将各种状态流程流转的实现分配到具体的方法中 <br>
     * 03 {@link StateHandler} stateMap 来处理存储不同的Status类实现。(从而免去了使⽤if判断各项状态转变的流程)<br>
     * 总结:<br>
     * 缺点: 产⽣了较多的实现类,代码实现上带来了较多的成本! <br>
     * 优点: 组件化 抽离了业务和非业务的的功能<br>
     *
     */
    @Test
    public void test_Editing2Arraignment() {
        String activityId = "100001";
        ActivityService.init(activityId, Status.Editing);

        StateHandler stateHandler = new StateHandler();
        Result result = stateHandler.arraignment(activityId, Status.Editing);

        log.info("测试结果(编辑中To提审活动)：{}", JSON.toJSONString(result));
        log.info("活动信息：{} 状态：{}", JSON.toJSONString(ActivityService.queryActivityInfo(activityId)), JSON.toJSONString(ActivityService.queryActivityInfo(activityId).getStatus()));
    }

    @Test
    public void test_Editing2Open() {
        String activityId = "100001";
        ActivityService.init(activityId, Status.Editing);

        StateHandler stateHandler = new StateHandler();
        Result result = stateHandler.open(activityId, Status.Editing);

        log.info("测试结果(编辑中To开启活动)：{}", JSON.toJSONString(result));
        log.info("活动信息：{} 状态：{}", JSON.toJSONString(ActivityService.queryActivityInfo(activityId)), JSON.toJSONString(ActivityService.queryActivityInfo(activityId).getStatus()));
    }

    @Test
    public void test_Refuse2Doing() {
        String activityId = "100001";
        ActivityService.init(activityId, Status.Refuse);

        StateHandler stateHandler = new StateHandler();
        Result result = stateHandler.doing(activityId, Status.Refuse);

        log.info("测试结果(拒绝To活动中)：{}", JSON.toJSONString(result));
        log.info("活动信息：{} 状态：{}", JSON.toJSONString(ActivityService.queryActivityInfo(activityId)), JSON.toJSONString(ActivityService.queryActivityInfo(activityId).getStatus()));
    }

    @Test
    public void test_Refuse2Revoke() {
        String activityId = "100001";
        ActivityService.init(activityId, Status.Refuse);

        StateHandler stateHandler = new StateHandler();
        Result result = stateHandler.checkRevoke(activityId, Status.Refuse);

        log.info("测试结果(拒绝To撤审)：{}", JSON.toJSONString(result));
        log.info("活动信息：{} 状态：{}", JSON.toJSONString(ActivityService.queryActivityInfo(activityId)), JSON.toJSONString(ActivityService.queryActivityInfo(activityId).getStatus()));
    }


}
