package com.max.demo.test;

import com.max.demo.Context;
import com.max.demo.impl.MJCouponDiscount;
import com.max.demo.impl.NYGCouponDiscount;
import com.max.demo.impl.ZJCouponDiscount;
import com.max.demo.impl.ZKCouponDiscount;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by lilei on 2021/11/16. <br>
 */
@Slf4j
public class ApiTest {


    /**
     * 策略模式<br>
     * 核心: 将方法中的if语句优化掉。<br>
     * 实现: <br>
     * 01 {@link com.max.demo.ICouponDiscount} 定义统一接口 以及抽象方法和可以处理不同类型参数的范型T <br>
     * 02 创建不同的实现: {@link com.max.demo.impl.MJCouponDiscount} {@link com.max.demo.impl.NYGCouponDiscount}... <br>
     * 03 创建{@link Context} 策略控制类 作用: 外部通过传递不同的策略实现, 再通过调用统一的方法执行策略计算 <br>
     * 总结： <br>
     * 01 去除了大量if-else判断 满足 隔离性与和扩展性 <br>
     * 02 注意: 策略模式 、 适配器模式、 组合模式等，在⼀些结构上是⽐较相似的。但是每⼀个模式是有⾃己的逻辑特点，
     * 在使用的过程中最佳的⽅式是经过较多的实践来吸取经验，为后续的研发设计提供更好的技术输出。
     */
    @Test
    public void test_zj() {
        // 直减；100-10，商品100元
        Context<Double> context = new Context<>(new ZJCouponDiscount());
        BigDecimal discountAmount = context.discountAmount(10D, new BigDecimal(100));
        log.info("测试结果：直减优惠后金额 {}", discountAmount);
    }

    @Test
    public void test_mj() {
        // 满100减10，商品100元
        Context<Map<String, String>> context = new Context<>(new MJCouponDiscount());
        Map<String, String> mapReq = new HashMap<>();
        mapReq.put("x", "100");
        mapReq.put("n", "10");
        BigDecimal discountAmount = context.discountAmount(mapReq, new BigDecimal(100));
        log.info("测试结果：满减优惠后金额 {}", discountAmount);
    }


    @Test
    public void test_zk() {
        // 折扣9折，商品100元
        Context<Double> context = new Context<>(new ZKCouponDiscount());
        BigDecimal discountAmount = context.discountAmount(0.9D, new BigDecimal(100));
        log.info("测试结果：折扣9折后金额 {}", discountAmount);
    }

    @Test
    public void test_nyg() {
        // n元购；100-10，商品100元
        Context<Double> context = new Context<>(new NYGCouponDiscount());
        BigDecimal discountAmount = context.discountAmount(90D, new BigDecimal(100));
        log.info("测试结果：n元购优惠后金额 {}", discountAmount);
    }

}
