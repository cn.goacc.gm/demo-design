package com.max.demo;

import java.math.BigDecimal;

/**
 * Created by lilei on 2021/11/16. <br>
 * 策略控制类
 * 策略模式的控制类主要是向外部可以传递不同的策略实现，再通过统一的⽅法执⾏优惠策略计算。
 * 另外这⾥也可以包装成map结构，让外部只需要对应的泛型类型即可使⽤用相应的服务
 */
public class Context<T> {

    private ICouponDiscount<T> couponDiscount;

    public Context(ICouponDiscount<T> couponDiscount) {
        this.couponDiscount = couponDiscount;
    }

    public BigDecimal discountAmount(T couponInfo, BigDecimal skuPrice) {
        return couponDiscount.discountAmount(couponInfo, skuPrice);
    }

}
