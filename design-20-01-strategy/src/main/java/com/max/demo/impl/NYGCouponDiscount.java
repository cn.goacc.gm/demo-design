package com.max.demo.impl;

import com.max.demo.ICouponDiscount;

import java.math.BigDecimal;

/**
 * Created by lilei on 2021/11/16. <br>
 * n元购买
 */
public class NYGCouponDiscount implements ICouponDiscount<Double> {

    /**
     * n元购购买
     * 1. 无论原价多少钱都固定金额购买
     */
    @Override
    public BigDecimal discountAmount(Double couponInfo, BigDecimal skuPrice) {
        return new BigDecimal(couponInfo);
    }
}
