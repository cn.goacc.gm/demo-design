package com.max.demo.impl;

import com.max.demo.ICouponDiscount;

import java.math.BigDecimal;

/**
 * Created by lilei on 2021/11/16. <br>
 * 直减
 */
public class ZJCouponDiscount implements ICouponDiscount<Double> {


    /**
     * 直减计算
     * 1. 使用商品价格减去优惠价格
     * 2. 最低支付金额1元
     */
    @Override
    public BigDecimal discountAmount(Double couponInfo, BigDecimal skuPrice) {
        BigDecimal discountAmount = skuPrice.subtract(BigDecimal.valueOf(couponInfo));
        if (discountAmount.compareTo(BigDecimal.ZERO) < 1) return BigDecimal.ONE;
        return discountAmount;
    }
}
