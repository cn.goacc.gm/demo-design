package com.max.demo;

import org.junit.Test;

/**
 * Created by lilei on 2021/9/10.
 */
public class ApiTest {


    /**
     * 步骤:
     * 01 定义统一的追加的方法 (定义装修包接⼝ (方法特点: 参数: 要存入的对象  返回值: `this`))  IMenu.class <p>
     * 02 实现该方法 处理业务逻辑 ( <p>
     * - 业务逻辑: 01 传入的对象存入List中、02 计算金额、<p>
     * - 属性参数: 面积和等级
     * ) <p>
     * 03 创建建造者 Builder.class <p>
     * - 通过不同的物料装修不同的风格
     */


    /**
     * 总结:
     * 01 使用场景: ⼀些基本物料不会变，而其组合经常变化的时候
     * 02 基本物料过多时或者组合过多时，可存储到数据库中。
     */

    @Test
    public void test() {

        Builder builder = new Builder();

        // 豪华欧式
        System.out.println(builder.levelOne(132.52D).getDetail());

        // 轻奢田园
        System.out.println(builder.levelTwo(98.25D).getDetail());

        // 现代简约
        System.out.println(builder.levelThree(85.43D).getDetail());

    }
}
