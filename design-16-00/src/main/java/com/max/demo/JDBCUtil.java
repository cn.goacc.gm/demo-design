package com.max.demo;

import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Created by lilei on 2021/10/11. <p>
 */
@Slf4j
public class JDBCUtil {

    public static final String URL = "jdbc:mysql://127.0.0.1:3306/max_demo_ddd";
    public static final String USER = "root";
    public static final String PASSWORD = "rootpass";

    public static void main(String[] args) throws Exception {
        Class.forName("com.mysql.cj.jdbc.Driver");
        //2. 获得数据库连接
        Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
        //3.操作数据库，实现增删改查
        Statement stmt = conn.createStatement();
        ResultSet resultSet = stmt.executeQuery("SELECT id, name, age, createTime, updateTime FROM user");
        //如果有数据，rs.next()返回true
        while (resultSet.next()) {
            log.info("测试结果 姓名：{} 年龄：{}", resultSet.getString("name"),resultSet.getInt("age"));
        }
    }

}
