package com.max.demo.test;

import com.alibaba.fastjson.JSON;
import com.max.demo.Activity;
import com.max.demo.ActivityController;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * Created by lilei on 2021/10/8. <p>
 */
@Slf4j
public class ApiTest {

    private ActivityController activityController = new ActivityController();

    @Test
    public void test_queryActivityInfo() {
        Long req = 10001L;
        Activity activity = activityController.queryActivityInfo(req);
        log.info("测试结果：{} {}", req, JSON.toJSONString(activity));
    }

}
