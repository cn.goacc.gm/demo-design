package com.max.demo.impl;

import com.max.demo.CacheService;
import com.max.demo.RedisUtils;
import com.max.demo.matter.EGM;
import com.max.demo.matter.IIR;

import java.util.concurrent.TimeUnit;

/**
 * Created by lilei on 2021/9/10.
 * <p>
 * 02 集群接口的定义
 */
public class CacheServiceImpl implements CacheService {

    private final RedisUtils redisUtils = new RedisUtils();

    private final EGM egm = new EGM();

    private final IIR iir = new IIR();

    @Override
    public String get(String key, int redisType) { //✨ 根据类型选择哪个集群
        if (1 == redisType) {
            return egm.gain(key);
        }
        if (2 == redisType) {
            return iir.get(key);
        }
        return redisUtils.get(key); //✨ 使用单机
    }

    @Override
    public void set(String key, String value, int redisType) {
        if (1 == redisType) {
            egm.set(key, value);
            return;
        }
        if (2 == redisType) {
            iir.set(key, value);
            return;
        }
        redisUtils.set(key, value);
    }


    @Override
    public void set(String key, String value, long timeout, TimeUnit timeUnit, int redisType) {
        if (1 == redisType) {
            egm.setEx(key, value, timeout, timeUnit);
            return;
        }
        if (2 == redisType) {
            iir.setExpire(key, value, timeout, timeUnit);
            return;
        }
        redisUtils.set(key, value, timeout, timeUnit);
    }

    @Override
    public void del(String key, int redisType) {
        if (1 == redisType) {
            egm.delete(key);
            return;
        }
        if (2 == redisType) {
            iir.del(key);
            return;
        }
        redisUtils.del(key);
    }


}
