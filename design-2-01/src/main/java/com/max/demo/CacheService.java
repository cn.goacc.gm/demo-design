package com.max.demo;

import java.util.concurrent.TimeUnit;

/**
 * Created by lilei on 2021/9/10.
 * 01 集群接口的定义
 */
public interface CacheService {

    String get(final String key, int redisType);

    void set(String key, String value, int redisType);

    void set(String key, String value, long timeout, TimeUnit timeUnit, int redisType);

    void del(String key, int redisType);
}
