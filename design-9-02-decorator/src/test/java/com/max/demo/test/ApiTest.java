package com.max.demo.test;

import com.max.demo.LoginSsoDecorator;
import com.max.demo.SsoInterceptor;
import org.junit.Test;

import java.io.BufferedReader;

/**
 * Created by lilei on 2021/10/8. <p>
 */
public class ApiTest {

    /**
     * 装饰器模式:
     * 01 在原有类基础上，给类新增功能。(其他方式: 继承、AOP切⾯)
     * 02 避免继承导致的⼦类过多
     * 03 示例: new BufferedReader(new FileReader("")); 字节流转字符流
     *
     * 实现:
     * 01 创建装饰类 <抽象类装饰角⾊> abstract class SsoDecorator{}
     *  001 继承接口 HandlerInterceptor
     *  002 创建私有HandlerInterceptor属性, 私有无参构造,提供公共有参构造
     *  003 覆盖方法接口方法(preHandle)
     * 02 创建装饰类的实现 <装饰角色逻辑实现> LoginSsoDecorator extends SsoDecorator
     *  001 创建与super 匹配的构造函数
     *  002 重写父类的preHandle
     *  003 在preHandle方法中处理业务逻辑, 可使用super.preHandle()语句前后横向扩展
     * 03 调用: 创建<装饰角色逻辑实现> LoginSsoDecorator(new SsoInterceptor()) 在构造方法中指定在 接口的实现
     *
     * 总结:
     * 使⽤装饰器模式满⾜ 单一职责原则 (在原有类进行扩展)
     * 重点是抽象类实现接口的使用, 同时接口的实现通过构造方法传递实现类
     */
    @Test
    public void test_LoginSsoDecorator() {
        LoginSsoDecorator ssoDecorator = new LoginSsoDecorator(new SsoInterceptor());
        String request = "1successhuahua";
        boolean success = ssoDecorator.preHandle(request, "ewcdqwt40liuiu", "t");
        System.out.println("登录校验：" + request + (success ? " 放行" : " 拦截"));

    }

}
