package com.max.demo;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * Created by lilei on 2021/10/11. <br>
 */
@Slf4j
public class ApiTest {


    /**
     * 观察者模式: <br>
     * 核心: <br>
     * 01 当某一行为发生时另一个用户做相应处理(两者之间没有耦合关系) <br>
     * 02 核心流程不会发生变化,但辅助流程随着业务发生变化(即辅助流程的处理需 等待 核心流程的结果) <br>
     * 实现: <br>
     * 01 {@link LotteryService#LotteryService()}业务抽象类构造方法: 对事件的定义 <br>
     * 02 {@link LotteryService#doDraw(String)}  protected abstract 方法
     */

    @Test
    public void test() {
        LotteryService lotteryService = new LotteryServiceImpl();
        LotteryResult result = lotteryService.draw("2765789109876");
        log.info("测试结果：{}", JSON.toJSONString(result));
    }

}
