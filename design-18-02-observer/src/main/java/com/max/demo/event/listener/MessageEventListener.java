package com.max.demo.event.listener;

import com.max.demo.LotteryResult;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by lilei on 2021/10/11. <br>
 */
@Slf4j
public class MessageEventListener implements EventListener {
    @Override
    public void doEvent(LotteryResult result) {
        log.info("给用户 {} 发送短信通知(短信)：{}", result.getUId(), result.getMsg());
    }

}
