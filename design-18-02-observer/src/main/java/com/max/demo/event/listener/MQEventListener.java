package com.max.demo.event.listener;

import com.max.demo.LotteryResult;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by lilei on 2021/10/11. <br>
 */
@Slf4j
public class MQEventListener implements EventListener {
    @Override
    public void doEvent(LotteryResult result) {
        log.info("记录用户 {} 摇号结果(MQ)：{}", result.getUId(), result.getMsg());
    }
}
