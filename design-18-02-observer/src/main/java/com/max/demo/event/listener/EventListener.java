package com.max.demo.event.listener;

import com.max.demo.LotteryResult;

/**
 * Created by lilei on 2021/10/11. <br>
 */
public interface EventListener {
    void doEvent(LotteryResult result);
}
