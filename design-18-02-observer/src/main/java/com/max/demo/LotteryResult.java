package com.max.demo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by lilei on 2021/10/11. <br>
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LotteryResult {
    /** 用户ID */
    private String uId;
    /** 摇号信息 */
    private String msg;
    /** 业务时间 */
    private Date dateTime;

}
