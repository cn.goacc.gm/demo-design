package com.max.demo.lang;

/**
 * Created by lilei on 2021/10/9. <p>
 */
public interface Collection<E, L> extends Iterable<E> {
    boolean add(E e);

    boolean remove(E e);

    boolean addLink(String key, L l);

    boolean removeLink(String key);

    Iterator<E> iterator();
}
