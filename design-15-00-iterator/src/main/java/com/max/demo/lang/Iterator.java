package com.max.demo.lang;

/**
 * Created by lilei on 2021/10/9. <p>
 */
public interface Iterator<E> {

    boolean hasNext();

    E next();

}
