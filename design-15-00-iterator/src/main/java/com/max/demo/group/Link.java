package com.max.demo.group;

import lombok.*;

/**
 * Created by lilei on 2021/10/9. <p>
 * 树节点链路
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Link {
    /** 雇员ID */
    private String fromId;
    /** 雇员ID */
    private String toId;

}
