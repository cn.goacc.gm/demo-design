package com.max.demo.group;

import lombok.Data;

/**
 * Created by lilei on 2021/10/9. <p>
 * 雇员
 */
@Data
public class Employee {
    /** ID */
    private String uId;
    /** 姓名 */
    private String name;
    /** 备注 */
    private String desc;

    public Employee(String name, String desc) {
        this.name = name;
        this.desc = desc;
    }

    public Employee(String uId, String name, String desc) {
        this.uId = uId;
        this.name = name;
        this.desc = desc;
    }
}
