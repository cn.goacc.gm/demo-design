package com.max.demo;

import lombok.Data;

/**
 * Created by lilei on 2021/10/9. <p>
 */
@Data
public class AuthInfo {

    private String code;
    private String info = "";

    public AuthInfo(String code, String ...infos) {
        this.code = code;
        for (String str:infos){
            this.info = this.info.concat(str);
        }
    }

}
