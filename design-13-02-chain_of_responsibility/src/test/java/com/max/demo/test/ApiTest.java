package com.max.demo.test;

import com.alibaba.fastjson.JSON;
import com.max.demo.AuthLink;
import com.max.demo.AuthService;
import com.max.demo.impl.Level1AuthLink;
import com.max.demo.impl.Level2AuthLink;
import com.max.demo.impl.Level3AuthLink;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.text.ParseException;
import java.util.Date;

/**
 * Created by lilei on 2021/10/9. <p>
 */
@Slf4j
public class ApiTest {


    /**
     * 责任链模式:
     * 实现:
     * 01 创建 {@link AuthLink}链路抽象类  <p>
     * - 001 链接起来的核⼼部分,属性: AuthLink next 来获取下一个审核节点信息  <p>
     * - 002 抽象方法{@link AuthLink#doAuth(String uId, String orderId, Date authDate)} 来处理不同基本的审核业务  <p>
     * - 003 实现{@link Level1AuthLink}、{@link Level2AuthLink}、{@link Level3AuthLink} ，实现了不同的审核级别处理的简单逻辑。  <p>
     * - 004 审核逻辑: 通过super.next(); 判断是否有下一个节点,没有则直接返回结果
     */
    @Test
    public void test_AuthLink() throws ParseException {
        AuthLink authLink = new Level3AuthLink("1000013", "王工")
                .appendNext(new Level2AuthLink("1000012", "张经理")
                        .appendNext(new Level1AuthLink("1000011", "段总")));

        log.info("测试结果：{}", JSON.toJSONString(authLink.doAuth("小傅哥", "1000998004813441", new Date())));

        // 模拟三级负责人审批
        AuthService.auth("1000013", "1000998004813441");
        log.info("测试结果：{}", "模拟三级负责人审批，王工");
        log.info("测试结果：{}", JSON.toJSONString(authLink.doAuth("小傅哥", "1000998004813441", new Date())));

        // 模拟二级负责人审批
        AuthService.auth("1000012", "1000998004813441");
        log.info("测试结果：{}", "模拟二级负责人审批，张经理");
        log.info("测试结果：{}", JSON.toJSONString(authLink.doAuth("小傅哥", "1000998004813441", new Date())));

        // 模拟一级负责人审批
        AuthService.auth("1000011", "1000998004813441");
        log.info("测试结果：{}", "模拟一级负责人审批，段总");
        log.info("测试结果：{}", JSON.toJSONString(authLink.doAuth("小傅哥", "1000998004813441", new Date())));

    }

}
