package com.max.demo.test;

import com.max.demo.EngineController;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * Created by lilei on 2021/9/29. <p>
 */
@Slf4j
public class ApiTest {

    @Test
    public void test_EngineController() {
        EngineController engineController = new EngineController();
        String process = engineController.process("Oli09pLkdjh", "man", 29);
        log.info("测试结果：{}", process);

    }
}
