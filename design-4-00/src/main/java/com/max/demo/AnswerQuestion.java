package com.max.demo;

import lombok.*;

/**
 * Created by lilei on 2021/9/10.
 * <p>
 * 解答题
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AnswerQuestion {

    private String name;  // 问题
    private String key;   // 答案
}
