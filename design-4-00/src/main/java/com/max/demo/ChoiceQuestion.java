package com.max.demo;

import lombok.*;

import java.util.Map;

/**
 * Created by lilei on 2021/9/10.
 * <p>
 * 单选题
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ChoiceQuestion {
    private String name;                 // 题目
    private Map<String, String> option;  // 选项；A、B、C、D
    private String key;                  // 答案；B


}
