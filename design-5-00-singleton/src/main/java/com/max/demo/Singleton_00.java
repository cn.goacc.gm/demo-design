package com.max.demo;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by lilei on 2021/9/28. <p>
 * 静态类使⽤
 * <p>
 * 01 常⻅的方式
 * <p>
 * 02 可以在类第一次加载的时候初始化
 */
public class Singleton_00 {

    public static Map<String, String> cache = new ConcurrentHashMap<>();

}
