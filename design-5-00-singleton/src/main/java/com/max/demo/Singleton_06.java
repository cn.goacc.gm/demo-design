package com.max.demo;

import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by lilei on 2021/9/28. <p>
 * CAS「AtomicReference」(线程安全) <p>
 * 实现: 通过AtomicReference封装引用一个示例 <p>
 * 总结:  <p>
 * 01 不需要使用传统的加锁方式保证线程安全，⽽是依赖于CAS的忙等算法 依赖于底层硬件的实现<p>
 * 02 当然CAS也有一个缺点就是忙等，如果一直没有获取到将会处于死循环中
 */
public class Singleton_06 {

    private static final AtomicReference<Singleton_06> INSTANCE = new AtomicReference<>();
    private static Singleton_06 instance;

    private Singleton_06() {
    }

    public static Singleton_06 getInstance() {
        for (; ; ) {
            Singleton_06 instance = INSTANCE.get();
            if (null != instance) return instance;
            INSTANCE.compareAndSet(null, new Singleton_06());
            return INSTANCE.get();
        }
    }

    public static void main(String[] args) {
        System.out.println(Singleton_06.getInstance());
        System.out.println(Singleton_06.getInstance());

    }

}
