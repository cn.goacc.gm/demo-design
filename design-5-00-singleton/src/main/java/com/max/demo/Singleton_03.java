package com.max.demo;

/**
 * Created by lilei on 2021/9/28. <p>
 * 饿汉模式(线程安全) <p>
 * 实现: <p>
 * 01 在定义属性时候就new对象<p>
 * 总结: <p>
 * 01 在类加载时候就会初始化 <p>
 * 02 不是懒加载
 */
public class Singleton_03 {

    private static Singleton_03 instance = new Singleton_03();

    private Singleton_03() {
    }

    public static Singleton_03 getInstance() {
        return instance;
    }

}
