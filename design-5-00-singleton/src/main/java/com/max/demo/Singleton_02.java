package com.max.demo;

/**
 * Created by lilei on 2021/9/28. <p>
 * 懒汉模式(线程安全)  <p>
 * 实现: 在外部访问的方法上加了synchronized锁 <p>
 * 总结: 虽然线程安全，但锁粒度太大
 */
public class Singleton_02 {

    private static Singleton_02 instance;

    private Singleton_02() {
    }

    public static synchronized Singleton_02 getInstance() {
        if (null != instance) return instance;
        instance = new Singleton_02();
        return instance;
    }
}
