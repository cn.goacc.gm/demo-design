package com.max.demo;

/**
 * Created by lilei on 2021/9/28. <p>
 * 使用类的内部类(线程安全) <p>
 * 实现: <p>
 * 01 构造方法私有化
 * 02 创建私有静态的私有类
 * 03 私有类创建私有静态的当前类属性并new
 * 04 创建外部方法直接返回私有类的属性
 */
public class Singleton_04 {

    private static class SingletonHolder {
        private static Singleton_04 instance = new Singleton_04();
    }

    private Singleton_04() {
    }

    public static Singleton_04 getInstance() {
        return SingletonHolder.instance;
    }

    public static void main(String[] args) {
        Singleton_04 instance = Singleton_04.getInstance();
        System.out.println(instance);
    }

}

