package com.max.demo;

/**
 * Created by lilei on 2021/9/28. <p>
 * 双重锁校验(线程安全) <p>
 * 实现: <p>
 * 01 在懒加载判断的地方添加synchronized锁 <p>
 * 总结: <p>
 * 01 双重锁的⽅式是方法级锁的优化，减少了了部分获取实例的耗时。 <p>
 * 02 同时这种⽅式也满⾜了懒加载
 */
public class Singleton_05 {

    private static Singleton_05 instance;

    private Singleton_05() {
    }

    public static Singleton_05 getInstance() {
        if (null != instance) return instance;

        synchronized (Singleton_05.class) {
            if (null == instance) {
                instance = new Singleton_05();
            }
        }

        return instance;
    }

}
