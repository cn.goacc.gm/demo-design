package com.max.demo;

/**
 * Created by lilei on 2021/9/28. <p>
 * 懒汉模式(线程不安全)
 * <p>
 * 实现方式:<p>
 * 01 创建私有静态当前类属性<p>
 * 02 构造方法私有化<p>
 * 03 提供公共静态外部访问方法<p>
 * 04 判断私有属性是否为空: 为空创建、不为空返回<p>
 * <p>
 * 总结: 线程不安全
 */
public class Singleton_01 {

    private static Singleton_01 instance;

    private Singleton_01() {

    }

    public static Singleton_01 getInstance() {
        if (instance != null) {
            return instance;
        }
        instance = new Singleton_01();
        return instance;
    }


}
