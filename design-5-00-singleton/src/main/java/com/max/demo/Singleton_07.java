package com.max.demo;

/**
 * Created by lilei on 2021/9/28. <p>
 * Effective Java作者推荐的枚举单例例(线程安全) <p>
 * 总结:
 * 绝对防⽌对此实例化，即使是在⾯对复杂的串⾏化或者反射攻击的时候,  <p>
 * 但是单元素的枚举类型已经成为实现Singleton的最佳⽅方法。 <p>
 * - 继承场景下是不可用的
 */
public enum Singleton_07 {

    INSTANCE;

    public void test() {
        System.out.println("hi~");
    }

}
