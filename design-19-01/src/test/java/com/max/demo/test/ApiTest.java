package com.max.demo.test;

import com.alibaba.fastjson.JSON;
import com.max.demo.ActivityExecStatusController;
import com.max.demo.ActivityService;
import com.max.demo.Result;
import com.max.demo.Status;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * Created by lilei on 2021/11/16. <br>
 */
@Slf4j
public class ApiTest {

    @Test
    public void test() {

        // 初始化数据
        String activityId = "100001";
        ActivityService.init(activityId, Status.Editing);

        ActivityExecStatusController activityExecStatusController = new ActivityExecStatusController();
        Result resultRefuse = activityExecStatusController.execStatus(activityId, Status.Editing, Status.Refuse);

        log.info("测试结果(编辑中To审核拒绝)：{}", JSON.toJSONString(resultRefuse));

        Result resultCheck = activityExecStatusController.execStatus(activityId, Status.Editing, Status.Check);
        log.info("测试结果(编辑中To提交审核)：{}", JSON.toJSONString(resultCheck));

    }


}
