package com.max.demo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by lilei on 2021/11/16. <br>
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Result {
    /** 编码 */
    private String code;
    /** 描述 */
    private String info;

}
