package com.max.demo;

/**
 * Created by lilei on 2021/10/11. <br>
 */
public interface LotteryService {
    LotteryResult doDraw(String uId);

}
