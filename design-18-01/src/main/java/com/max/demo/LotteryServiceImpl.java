package com.max.demo;

import lombok.extern.slf4j.Slf4j;

import java.util.Date;

/**
 * Created by lilei on 2021/10/11. <br>
 */
@Slf4j
public class LotteryServiceImpl implements LotteryService {

    private final MinibusTargetService minibusTargetService = new MinibusTargetService();

    @Override
    public LotteryResult doDraw(String uId) {
        // 摇号
        String lottery = minibusTargetService.lottery(uId);
        // 发短信
        log.info("给用户 {} 发送短信通知(短信)：{}", uId, lottery);
        // 发MQ消息
        log.info("记录用户 {} 摇号结果(MQ)：{}", uId, lottery);
        // 结果
        return new LotteryResult(uId, lottery, new Date());
    }

}
