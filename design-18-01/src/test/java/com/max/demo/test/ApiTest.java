package com.max.demo.test;

import com.alibaba.fastjson.JSON;
import com.max.demo.LotteryResult;
import com.max.demo.LotteryService;
import com.max.demo.LotteryServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * Created by lilei on 2021/10/11. <br>
 */
@Slf4j
public class ApiTest {

    @Test
    public void test() {
        LotteryService lotteryService = new LotteryServiceImpl();
        LotteryResult result = lotteryService.doDraw("2765789109876");
        log.info("测试结果：{}", JSON.toJSONString(result));
    }

}
