package com.max.demo.service;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by lilei on 2021/9/28. <p>
 */
@Slf4j
public class POPOrderService {

    public boolean isFirstOrder(String uId) {
        log.info("POP商家，查询用户的订单是否为首单：{}", uId);
        return true;
    }
}
