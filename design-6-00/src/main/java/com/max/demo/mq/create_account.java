package com.max.demo.mq;

import com.alibaba.fastjson.JSON;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

/**
 * Created by lilei on 2021/9/28. <p>
 * 开户
 */
@Setter
@Getter
public class create_account {
    /** 开户编号 */
    private String number;
    /** 开户地 */
    private String address;
    /** 开户时间 */
    private Date accountDate;
    /** 开户描述 */
    private String desc;

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
