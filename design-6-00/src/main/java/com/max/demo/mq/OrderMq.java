package com.max.demo.mq;

import com.alibaba.fastjson.JSON;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * Created by lilei on 2021/9/28. <p>
 */
@Setter
@Getter
public class OrderMq {
    /** 用户ID */
    private String uid;
    /** 商品 */
    private String sku;
    /** 订单ID */
    private String orderId;
    /** 下单时间 */
    private Date createOrderTime;

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }

}
