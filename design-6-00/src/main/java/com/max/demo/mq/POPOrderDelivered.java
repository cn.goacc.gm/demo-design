package com.max.demo.mq;

import com.alibaba.fastjson.JSON;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by lilei on 2021/9/28. <p>
 * 订单妥投消息
 */
@Setter
@Getter
public class POPOrderDelivered {
    /** 用户ID */
    private String uId;
    /** 订单号 */
    private String orderId;
    /** 下单时间 */
    private Date orderTime;
    /** 商品 */
    private Date sku;
    /** 商品名称 */
    private Date skuName;
    /** 金额 */
    private BigDecimal decimal;

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
