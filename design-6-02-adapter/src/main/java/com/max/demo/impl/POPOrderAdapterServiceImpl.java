package com.max.demo.impl;

import com.max.demo.OrderAdapterService;
import com.max.demo.service.OrderService;
import com.max.demo.service.POPOrderService;

/**
 * Created by lilei on 2021/9/28. <p>
 */
public class POPOrderAdapterServiceImpl implements OrderAdapterService {
    private POPOrderService popOrderService = new POPOrderService();

    @Override
    public boolean isFirst(String uId) {
        return popOrderService.isFirstOrder(uId);
    }
}
