package com.max.demo.impl;

import com.max.demo.OrderAdapterService;
import com.max.demo.service.OrderService;

/**
 * Created by lilei on 2021/9/28. <p>
 */
public class InsideOrderService implements OrderAdapterService {

    private OrderService orderService = new OrderService();

    @Override
    public boolean isFirst(String uId) {
        return orderService.queryUserOrderCount(uId) <= 1;
    }
}
