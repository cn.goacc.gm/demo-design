package com.max.demo;

import lombok.*;

import java.util.Date;

/**
 * Created by lilei on 2021/9/28. <p>
 * 统一的MQ消息体 <p>
 * 定义了通用的MQ消息体,后续把接入进来的消息进行统⼀的处理(返回一样数据格式)
 */
@Data
public class RebateInfo {

    /** 用户ID */
    private String userId;
    /** 业务ID */
    private String bizId;
    /** 业务时间 */
    @Setter(AccessLevel.NONE)
    private Date bizTime;
    /** 业务描述 */
    private String desc;

    public void setBizTime(Date bizTime) {
        this.bizTime = bizTime;
    }

    public void setBizTime(String bizTime) {
        this.bizTime = new Date(Long.parseLong("1591077840669"));
    }

}
