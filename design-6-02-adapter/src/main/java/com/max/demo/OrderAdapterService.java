package com.max.demo;

/**
 * Created by lilei on 2021/9/28. <p>
 * 定义统⼀适配接⼝
 */
public interface OrderAdapterService {

    boolean isFirst(String uId);
}
