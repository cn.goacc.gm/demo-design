package com.max.demo;

import com.alibaba.fastjson.JSON;
import com.max.demo.impl.InsideOrderService;
import com.max.demo.impl.POPOrderAdapterServiceImpl;
import com.max.demo.mq.OrderMq;
import com.max.demo.mq.create_account;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.ParseException;
import java.util.HashMap;

/**
 * Created by lilei on 2021/9/28. <p>
 */
public class ApiTest {


    /**
     * 01 MQ消息适配(类) <p>
     * 实现: <p>
     * 01 定义通用的MQ消息体 <p>
     * 02 创建MQ消息体适配类 MQAdapter
     * 03 因为mq消息基本都是 json格式, 在第一个filter方法中将Json转成Map结构  filter(String strJson, Map<String, String> link)
     * 04 第二个重载方法filter(Map obj, Map<String, String> link)          link参数: (映射成属性名,要映射的属性名)  (<-)
     * 05 通过反射封装映射(也可以通过代理的方式)
     * 总结: <p>
     * 01 定义通用的MQ消息体,后续传过来的消息进行统一处理
     * 02 真实环境中可将配置关系交给配置⽂件或者数据库后台配置
     */
    @Test
    public void test_MQAdapter() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, ParseException {
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date parse = s.parse("2020-06-01 23:20:16");

        create_account create_account = new create_account();
        create_account.setNumber("100001");
        create_account.setAddress("河北省.廊坊市.广阳区.大学里职业技术学院");
        create_account.setAccountDate(parse);
        create_account.setDesc("在校开户");

        HashMap<String, String> link01 = new HashMap<String, String>();
        link01.put("userId", "number");
        link01.put("bizId", "number");
        link01.put("bizTime", "accountDate");
        link01.put("desc", "desc");
        RebateInfo rebateInfo01 = MQAdapter.filter(create_account.toString(), link01);
        System.out.println("mq.create_account(适配前)" + create_account.toString());
        System.out.println("mq.create_account(适配后)" + JSON.toJSONString(rebateInfo01));

        System.out.println("");

        OrderMq orderMq = new OrderMq();
        orderMq.setUid("100001");
        orderMq.setSku("10928092093111123");
        orderMq.setOrderId("100000890193847111");
        orderMq.setCreateOrderTime(parse);

        HashMap<String, String> link02 = new HashMap<String, String>();
        link02.put("userId", "uid");
        link02.put("bizId", "orderId");
        link02.put("bizTime", "createOrderTime");
        RebateInfo rebateInfo02 = MQAdapter.filter(orderMq.toString(), link02);
        System.out.println("mq.orderMq(适配前)" + orderMq.toString());
        System.out.println("mq.orderMq(适配后)" + JSON.toJSONString(rebateInfo02));
    }


    /**
     * 01 接口适配(方法(返回值))
     * 实现:
     * 01 定义统一适配接⼝ OrderAdapterService
     * 02 分别实现OrderAdapterService接口
     * 03 方法体中 调用需要适配的服务
     * 04 调用时,new不同的实现即可 返回统一的数据类型
     */
    @Test
    public void test_itfAdapter() {
        OrderAdapterService popOrderAdapterService = new POPOrderAdapterServiceImpl();
        System.out.println("判断⾸首单，接⼝口适配(POP):" + popOrderAdapterService.isFirst("100001"));
        OrderAdapterService insideOrderService = new InsideOrderService();
        System.out.println("判断⾸首单，接⼝口适配(⾃自营):" + insideOrderService.isFirst("100001"));
    }


}
