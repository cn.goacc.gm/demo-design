package com.max.demo.agent;

import java.lang.annotation.*;

/**
 * Created by lilei on 2021/10/8. <p>
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface Select {
    String value() default "";
}
