package com.max.demo;

import com.max.demo.agent.Select;

/**
 * Created by lilei on 2021/10/8. <p>
 */
public interface IUserDao {
    @Select("select userName from user where id = #{uId}")
    String queryUserInfo(String uId);

}
