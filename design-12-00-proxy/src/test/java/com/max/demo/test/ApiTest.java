package com.max.demo.test;

import com.max.demo.IUserDao;
import com.max.demo.agent.MapperFactoryBean;
import com.max.demo.agent.RegisterBeanFactory;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by lilei on 2021/10/8. <p>
 */
@Slf4j
public class ApiTest {

    /**
     * 代理模式:
     * 实现:
     * 01 通过实现Spring中的FactoryBean方法中getObject方法,返回InvocationHandler实例,做到接口代理对象{@link MapperFactoryBean}
     * 02 通过Bean工厂注册到Spring中 {@link RegisterBeanFactory}
     * 03 通过配置文件将工厂加载到容器中 {@link spring-config.xml}
     */
    @Test
    public void test_IUserDao() {
        BeanFactory beanFactory = new ClassPathXmlApplicationContext("spring-config.xml");
        IUserDao userDao = beanFactory.getBean("userDao", IUserDao.class);
        String res = userDao.queryUserInfo("100001");
        log.info("测试结果：{}", res);
    }

}
