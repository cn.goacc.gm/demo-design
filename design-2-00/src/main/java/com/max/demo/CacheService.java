package com.max.demo;

import java.util.concurrent.TimeUnit;

/**
 * Created by lilei on 2021/9/10.
 * <p>
 * 04 单集群代码
 */
public interface CacheService {

    String get(final String key);

    void set(String key, String value);

    void set(String key, String value, long timeout, TimeUnit timeUnit);

    void del(String key);

}
