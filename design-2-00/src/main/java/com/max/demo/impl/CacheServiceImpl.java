package com.max.demo.impl;

import com.max.demo.CacheService;
import com.max.demo.RedisUtils;

import java.util.concurrent.TimeUnit;

/**
 * Created by lilei on 2021/9/10.
 * <p>
 * 05 实现调⽤
 */
public class CacheServiceImpl implements CacheService {

    private final RedisUtils redisUtils = new RedisUtils();

    @Override
    public String get(String key) {
        return redisUtils.get(key);
    }

    public void set(String key, String value) {
        redisUtils.set(key, value);
    }

    public void set(String key, String value, long timeout, TimeUnit timeUnit) {
        redisUtils.set(key, value, timeout, timeUnit);
    }

    public void del(String key) {
        redisUtils.del(key);
    }

}
