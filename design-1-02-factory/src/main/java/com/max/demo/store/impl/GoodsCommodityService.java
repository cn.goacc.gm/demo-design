package com.max.demo.store.impl;

import com.alibaba.fastjson.JSON;
import com.max.demo.goods.DeliverReq;
import com.max.demo.goods.GoodsService;
import com.max.demo.store.ICommodity;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.logging.Log;

import java.util.Map;

/**
 * Created by lilei on 2021/9/10.
 * 01 实物商品 - 实现奖品发放接口
 */
@Slf4j
public class GoodsCommodityService implements ICommodity {
    public void sendCommodity(String uId, String commodityId, String bizId, Map<String, String> extMap) throws Exception {
        GoodsService goodsService = new GoodsService();
        DeliverReq deliverReq = DeliverReq.builder()
                .userName(queryUserName(uId))
                .userPhone(queryUserPhoneNumber(uId))
                .sku(commodityId)
                .orderId(bizId)
                .consigneeUserName(extMap.get("consigneeUserName")) // 收货信息
                .consigneeUserPhone(extMap.get("consigneeUserPhone"))
                .consigneeUserAddress(extMap.get("consigneeUserAddress")).build();
        Boolean isSuccess = goodsService.deliverGoods(deliverReq);
        log.info("请求参数[优惠券] => uId：{} commodityId：{} bizId：{} extMap：{}", uId, commodityId, bizId, JSON.toJSON(extMap));
        log.info("测试结果[优惠券]：{}", isSuccess);

        if (!isSuccess) throw new RuntimeException("实物商品发放失败");
    }

    private String queryUserName(String uId) {
        return "花花";
    }

    private String queryUserPhoneNumber(String uId) {
        return "15200101232";
    }
}
