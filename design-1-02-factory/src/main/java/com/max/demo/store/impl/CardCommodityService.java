package com.max.demo.store.impl;

import com.alibaba.fastjson.JSON;
import com.max.demo.card.IQiYiCardService;
import com.max.demo.store.ICommodity;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * Created by lilei on 2021/9/10.
 * 03 第三⽅兑换卡 - 实现奖品发放接⼝
 */
@Slf4j
public class CardCommodityService implements ICommodity {

    // 模拟注入
    private final IQiYiCardService iQiYiCardService = new IQiYiCardService();

    @Override
    public void sendCommodity(String uId, String commodityId, String bizId, Map<String, String> extMap) throws Exception {
        String mobile = queryUserMobile(uId);
        iQiYiCardService.grantToken(mobile, bizId);
        log.info("请求参数[爱奇艺兑换卡] => uId：{} commodityId：{} bizId：{} extMap：{}", uId, commodityId, bizId, JSON.toJSON(extMap));
        log.info("测试结果[爱奇艺兑换卡]：success");

    }

    private String queryUserMobile(String uId) {
        return "15200101232";
    }
}
