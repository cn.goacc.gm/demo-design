package com.max.demo.store;

import java.util.Map;

/**
 * Created by lilei on 2021/9/10.
 * 01 发奖接⼝口
 */
public interface ICommodity {

    void sendCommodity(String uId, String commodityId, String bizId, Map<String, String> extMap) throws Exception;
}
