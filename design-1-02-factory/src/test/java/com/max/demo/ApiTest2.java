package com.max.demo;

import com.max.demo.store.ICommodity;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by lilei on 2021/9/10.
 * <p>
 * 01 创建者模式-工厂方法模式
 */
public class ApiTest2 {

    /**
     * 实现步骤:
     * 01 定义统一的接口 (定义发奖接⼝: 无论是实物、虚拟还是第三方, 都从该接口去找实现(保证最终入参出参的统一性))  ICommodity.java
     * 02 实现该接口
     * CardCommodityService.java   CouponCommodityService.java  GoodsCommodityService.java
     * <p>
     * 03 在实现类中处理业务逻辑 (处理发放奖品的内部逻辑)
     * sendCommodity方法
     * <p>
     * 04 创建商店工⼚类 (提供一个根据 类型 而返回`ICommodity接口实现类`)
     * StoreFactory.java
     * public ICommodity getCommodityService(Integer commodityType)
     *
     */

    /**
     * 总结:
     * 01 `避免创建者与具体的产品逻辑耦合` 满⾜`单一职责`，每⼀一个业务逻辑实现都在所属⾃自⼰己的类中完成
     * 02 `满⾜足开闭原则` 调用方无需修改原有代码 便可添加需求
     */

    /**
     * 问题:
     * ⽐如有⾮常多的奖品类型，那么实现的⼦类会极速扩张。因此也需要使⽤其他的模式进行优化
     */

    @Test
    public void test_commodity() throws Exception {
        StoreFactory storeFactory = new StoreFactory();

        // 1. 优惠券
        ICommodity commodityService_1 = storeFactory.getCommodityService(1);
        commodityService_1.sendCommodity("10001", "EGM1023938910232121323432", "791098764902132", null);

        // 2. 实物商品
        ICommodity commodityService_2 = storeFactory.getCommodityService(2);
        Map<String, String> extMap = new HashMap<String, String>();
        extMap.put("consigneeUserName", "谢飞机");
        extMap.put("consigneeUserPhone", "15200292123");
        extMap.put("consigneeUserAddress", "吉林省.长春市.双阳区.XX街道.檀溪苑小区.#18-2109");
        commodityService_2.sendCommodity("10001", "9820198721311", "1023000020112221113", new HashMap<String, String>() {{
            put("consigneeUserName", "谢飞机");
            put("consigneeUserPhone", "15200292123");
            put("consigneeUserAddress", "吉林省.长春市.双阳区.XX街道.檀溪苑小区.#18-2109");
        }});

        // 3. 第三方兑换卡(爱奇艺)
        ICommodity commodityService_3 = storeFactory.getCommodityService(3);
        commodityService_3.sendCommodity("10001", "AQY1xjkUodl8LO975GdfrYUio", null, null);

    }

}
