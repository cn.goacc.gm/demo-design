package com.max.demo.test;

import com.max.demo.LoginSsoDecorator;
import org.junit.Test;

/**
 * Created by lilei on 2021/10/8. <p>
 */
public class ApiTest {

    @Test
    public void test_LoginSsoDecorator() {
        LoginSsoDecorator ssoDecorator = new LoginSsoDecorator();
        String request = "1successhuahua";
        boolean success = ssoDecorator.preHandle(request, "ewcdqwt40liuiu", "t");
        System.out.println("登录校验:" + request + (success ? " 放⾏" : " 拦截"));
    }

}
