package com.max.demo;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by lilei on 2021/10/9. <p>
 */
public class AuthController {

    private SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 时间格式化

    /**
     * queryAuthInfo(uid,orderId)
     * logic:
     * 01 uid 控制着该单号是否审批
     * 02 当通过uid和单号未查到Date时候(date is null) 通过 new AuthInfo() 来指定审批人
     * 03 ApiTest 通过 AuthService.auth(uid, orderId); 来审批通过(uid手动指定的)
     */
    public AuthInfo doAuth(String uId, String orderId, Date authDate) throws ParseException {

        // 三级审批
        Date date = AuthService.queryAuthInfo("1000013", orderId);
        if (null == date) return new AuthInfo("0001", "单号：", orderId, " 状态：待三级审批负责人 ", "王工");

        // 二级审批 (2021-10-09 10:51:49 > 2021-10-01 00:00:00  &&  2021-10-09 10:51:49 < 2021-10-25 23:59:59  )
        if (authDate.after(f.parse("2021-10-01 00:00:00")) && authDate.before(f.parse("2021-10-25 23:59:59"))) {
            date = AuthService.queryAuthInfo("1000012", orderId);
            if (null == date) return new AuthInfo("0001", "单号：", orderId, " 状态：待二级审批负责人 ", "张经理");
        }

        // 一级审批
        if (authDate.after(f.parse("2021-10-9 00:00:00")) && authDate.before(f.parse("2021-10-20 23:59:59"))) {
            date = AuthService.queryAuthInfo("1000011", orderId);
            if (null == date) return new AuthInfo("0001", "单号：", orderId, " 状态：待一级审批负责人 ", "段总");
        }

        return new AuthInfo("0001", "单号：", orderId, " 状态：审批完成");
    }

}
